from fastapi import APIRouter, status
from pydantic import BaseModel
from datetime import datetime
from typing import Optional


router = APIRouter(
  prefix = '/entries',
  tags = [ 'entries' ],
  responses = { status.HTTP_404_NOT_FOUND: { 'description' : 'Not found' }},
)


@router.get('/summary')
async def get_summary(
  granularity: str,
  date: Optional[str] = None,
  start: Optional[str] = None,
  end: Optional[str] = None):
  return {
    'message': 'generated summary for granularity %s' % granularity,
    'meta': {
      'date': date,
      'start': start,
      'end': end
    }
  }
