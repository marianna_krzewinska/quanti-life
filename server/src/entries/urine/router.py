from fastapi import APIRouter, Depends, status
from datetime import datetime
from typing import Optional, List
from sqlalchemy.orm import relationship, Session

from . import models, methods, schemas
from database import SessionLocal, engine, get_db
from auth import authorize


models.Base.metadata.create_all(bind = engine)


router = APIRouter(
  prefix = '/entries/urine',
  tags = [ 'urine entries' ],
  responses = { status.HTTP_404_NOT_FOUND: { 'description' : 'Not found' } },
)


@router.get('', response_model = List[schemas.UrineEntryOut])
async def get_urine_entries(
  db: Session = Depends(get_db),
  auth = Depends(authorize)):

  return methods.get_urine_entries(db, auth['user'].id)


@router.get('/summary', response_model = schemas.SummaryOut)
async def get_aggregated_urine_entries(
  start: datetime,
  end: datetime,
  db: Session = Depends(get_db),
  auth = Depends(authorize)):

  entries = methods.get_aggregated_urine_entries(db, auth['user'].id, start, end)

  return {
    'start': start,
    'end': end,
    'aggregates': entries,
  }


@router.post('', response_model = schemas.UrineEntryOut)
async def add_urine_entry(
  entry: schemas.UrineEntryData,
  db: Session = Depends(get_db),
  auth = Depends(authorize)):

  return methods.create_urine_entry(db, entry, auth['user'].id)


@router.put('/{entry_id}', response_model = schemas.UrineEntryOut)
async def update_urine_entry(
  entry_id: str,
  entry: schemas.UrineEntryData,
  db: Session = Depends(get_db),
  auth = Depends(authorize)):

  return methods.update_urine_entry(db, entry, entry_id, auth['user'].id)


@router.delete('/{entry_id}', response_model = int)
async def delete_urine_entry(
  entry_id: str,
  db: Session = Depends(get_db),
  auth = Depends(authorize)):

  return methods.delete_urine_entry(db, entry_id, auth['user'].id)
