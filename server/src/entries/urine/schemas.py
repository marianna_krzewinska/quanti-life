from pydantic import BaseModel
from datetime import datetime
import uuid as uuid
from typing import Optional, List

class UrineEntryData(BaseModel):
  volume: int
  color: str
  timestamp: datetime


class UrineEntryOut(BaseModel):
  id: uuid.UUID
  volume: int
  color: str
  user_id: Optional[uuid.UUID]
  timestamp: datetime
  updated_at: datetime
  created_at: datetime

  class Config:
    orm_mode = True


class Aggregate(BaseModel):
  volume: int
  timestamp: datetime
  color: str


class SummaryOut(BaseModel):
  start: datetime
  end: datetime
  aggregates: List[Aggregate]
