from sqlalchemy.orm import Session
import uuid as uuid
from fastapi import HTTPException, status
from datetime import datetime

from . import schemas, models


def get_urine_entries(db: Session, user_id: uuid.UUID):
  return db.query(models.UrineEntry) \
    .filter_by(user_id = user_id) \
    .all()


def get_aggregated_urine_entries(
  db: Session,
  user_id: uuid.UUID,
  start: datetime,
  end: datetime):

  if (start > end): raise HTTPException(
    status_code = status.HTTP_400_BAD_REQUEST,
    detail = 'Bad request - start date can\'t be after end date')

  granularity = 'day' if (end - start).days > 0 else 'hours'

  entries = db.execute('''
    select
      sum(volume),
      date_trunc('{granularity}', timestamp) as granularity,
      color
    from urine_entries
    where user_id = '{user_id}' and timestamp >= '{start}' and timestamp <= '{end}'
    group by granularity, color
    order by granularity;
  '''.format(
    user_id = user_id,
    start = start,
    end = end,
    granularity = granularity))

  result = [{
      'volume': entry[0],
      'timestamp': entry[1],
      'color': entry[2]
    } for entry in entries]

  return result


def create_urine_entry(db: Session, entry: schemas.UrineEntryData, user_id: str):
  db_entry = models.UrineEntry(
    volume = entry.volume,
    color = entry.color,
    timestamp = entry.timestamp,
    user_id = user_id,
  )

  db.add(db_entry)
  db.commit()
  db.refresh(db_entry)

  return db_entry


def update_urine_entry(
  db: Session,
  entry_data: schemas.UrineEntryData,
  entry_id: uuid.UUID,
  user_id: uuid.UUID):

  db_entry = db.query(models.UrineEntry) \
    .filter_by(id = entry_id, user_id = user_id)

  db_entry.update(dict(entry_data))

  updated_entry = db_entry.first()

  if (updated_entry == None):
    db.rollback()

    raise HTTPException(
      status_code = status.HTTP_404_NOT_FOUND,
      detail = 'Urine Entry not found')

  db.commit()

  return updated_entry


def delete_urine_entry(db: Session, entry_id: uuid.UUID, user_id: uuid.UUID):
  result = db.query(models.UrineEntry) \
    .filter_by(id = entry_id, user_id = user_id) \
    .delete()

  if (result == 1):
    db.commit()
    return

  exception = None

  if (result == 0):
    exception = HTTPException(
      status_code = status.HTTP_404_NOT_FOUND,
      detail = 'Urine Entry not found')

  elif (result > 1):
    exception = HTTPException(
      status_code = status.HTTP_400_BAD_REQUEST,
      detail = 'Bad request - too many entries matched')

  else:
    exception = HTTPException(
      status_code = status.HTTP_400_BAD_REQUEST,
      detail = 'Bad request - failed to delete entry %s' % entry_id)

  db.rollback()

  raise exception
