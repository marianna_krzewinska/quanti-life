from pydantic import BaseModel, conint
from datetime import datetime
import uuid as uuid
from typing import Optional, List


class WaterEntryData(BaseModel):
  volume: int
  timestamp: datetime


class WaterEntryOut(BaseModel):
  id: uuid.UUID
  volume: conint(multiple_of = 100, ge = 100, le = 300)
  user_id: Optional[uuid.UUID]
  timestamp: datetime
  updated_at: datetime
  created_at: datetime

  class Config:
    orm_mode = True


class Aggregate(BaseModel):
  volume: int
  timestamp: datetime


class SummaryOut(BaseModel):
  start: datetime
  end: datetime
  aggregates: List[Aggregate]
