from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, Session
from datetime import datetime
import uuid

from database import Base


class WaterEntry(Base):
  __tablename__ = 'water_entries'

  id = Column(UUID(as_uuid = True), primary_key = True, default = uuid.uuid4)
  volume = Column(Integer, nullable = False)
  timestamp = Column(DateTime, nullable = False)
  user_id = Column(UUID(as_uuid = True), nullable = True)

  created_at = Column(DateTime, nullable = False, default = datetime.now)
  updated_at = Column(
    DateTime,
    nullable = False,
    onupdate = datetime.now,
    default = datetime.now)
