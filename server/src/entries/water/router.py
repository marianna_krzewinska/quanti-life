from fastapi import APIRouter, Depends, status
from datetime import datetime
from typing import Optional, List
from sqlalchemy.orm import relationship, Session

from . import models, methods, schemas
from database import SessionLocal, engine, get_db
from auth import authorize


models.Base.metadata.create_all(bind = engine)


router = APIRouter(
  prefix = '/entries/water',
  tags = [ 'water entries' ],
  responses = { status.HTTP_404_NOT_FOUND: { 'description' : 'Not found' } },
)


@router.get('', response_model = List[schemas.WaterEntryOut])
async def get_water_entries(
  db: Session = Depends(get_db),
  auth = Depends(authorize)):

  return methods.get_water_entries(db, auth['user'].id)


@router.post('', response_model = schemas.WaterEntryOut)
async def add_water_entry(
  entry: schemas.WaterEntryData,
  auth = Depends(authorize),
  db: Session = Depends(get_db)):

  return methods.create_water_entry(db, entry, auth['user'].id)


@router.get('/summary', response_model = schemas.SummaryOut)
async def get_aggregated_water_entries(
  start: datetime,
  end: datetime,
  db: Session = Depends(get_db),
  auth = Depends(authorize)):

  entries = methods.get_aggregated_water_entries(
    db,
    auth['user'].id,
    start,
    end)

  return {
    'start': start,
    'end': end,
    'aggregates': entries,
  }


@router.put('/{entry_id}', response_model = schemas.WaterEntryOut)
async def update_water_entry(
  entry_id: str,
  entry: schemas.WaterEntryData,
  auth = Depends(authorize),
  db: Session = Depends(get_db)):

  return methods.update_water_entry(db, entry, entry_id, auth['user'].id)


@router.delete('/{entry_id}', response_model = int)
async def delete_water_entry(
  entry_id: str,
  db: Session = Depends(get_db),
  auth = Depends(authorize)):

  return methods.delete_water_entry(db, entry_id, auth['user'].id)
