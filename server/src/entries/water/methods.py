from sqlalchemy.orm import Session
from sqlalchemy import func
from fastapi import HTTPException, status
from datetime import datetime
import uuid as uuid

from . import schemas, models


def get_water_entries(db: Session, user_id: uuid.UUID):
  return db.query(models.WaterEntry) \
    .filter_by(user_id = user_id) \
    .all()


def get_aggregated_water_entries(
  db: Session,
  user_id: uuid.UUID,
  start: datetime,
  end: datetime):

  if (start > end): raise HTTPException(
    status_code = status.HTTP_400_BAD_REQUEST,
    detail = 'Bad request - start date can\'t be after end date')

  granularity = 'day' if (end - start).days > 0 else 'hours'

  entries = db.execute('''
    select sum(volume), date_trunc('{granularity}', timestamp) as granularity
    from water_entries
    where user_id = '{user_id}' and timestamp >= '{start}' and timestamp <= '{end}'
    group by granularity
    order by granularity;
  '''.format(
    user_id = user_id,
    start = start,
    end = end,
    granularity = granularity))

  result = [{ 'volume': entry[0], 'timestamp': entry[1] } for entry in entries]

  return result

def create_water_entry(
  db: Session,
  entry: schemas.WaterEntryData,
  user_id: uuid.UUID):

  db_entry = models.WaterEntry(
    volume = entry.volume,
    user_id = user_id,
    timestamp = entry.timestamp,
  )

  db.add(db_entry)
  db.commit()
  db.refresh(db_entry)

  return db_entry


def update_water_entry(
  db: Session,
  entry_data: schemas.WaterEntryData,
  entry_id: uuid.UUID,
  user_id: uuid.UUID):
  db_entry = db.query(models.WaterEntry) \
    .filter_by(id = entry_id, user_id = user_id)

  db_entry.update(dict(entry_data))

  db.commit()

  entry = db_entry.first()

  if (entry == None):
    raise HTTPException(
      status_code = status.HTTP_404_NOT_FOUND,
      detail = 'Water Entry not found')

  return entry


def delete_water_entry(db: Session, entry_id: str, user_id: uuid.UUID):
  result = db.query(models.WaterEntry) \
    .filter_by(id = entry_id, user_id = user_id) \
    .delete()

  if (result == 1):
    db.commit()
    return

  exception = None

  if (result == 0):
    exception = HTTPException(
      status_code = status.HTTP_404_NOT_FOUND,
      detail = 'Water Entry not found')

  elif (result > 1):
    exception = HTTPException(
      status_code = status.HTTP_400_BAD_REQUEST,
      detail = 'Bad request - too many entries matched')

  else:
    exception = HTTPException(
      status_code = status.HTTP_400_BAD_REQUEST,
      detail = 'Bad request - failed to delete entry %s' % entry_id)

  db.rollback()

  raise exception
