from fastapi import APIRouter, Depends, Header, Response, status
import httpx
from database import SessionLocal, engine, get_db
from . import methods, schemas
from sqlalchemy.orm import Session
from typing import Optional, List
from auth import authorize


router = APIRouter(
  prefix = '/users',
  tags = [ 'users' ],
  responses = { status.HTTP_404_NOT_FOUND: { 'description' : 'Not found' }},
)

@router.get('/me', response_model = schemas.UserOut)
async def get_my_profile(
  response: Response,
  db: Session = Depends(get_db),
  auth = Depends(authorize),
  authorization: str = Header(...)):

  response.headers['content-type'] = 'application/json; charset=utf-8'

  if (auth['user'] != None):
    return auth['user']

  return await methods.create_user(db, auth['tokeninfo'], authorization)
