from sqlalchemy import Boolean, Column, String, DateTime, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, Session
from datetime import datetime
import uuid

from database import Base

class User(Base):
  __tablename__ = 'users'

  id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
  email = Column(String, nullable=False)
  first_name = Column(String, nullable=False)
  last_name = Column(String, nullable=False)
  display_name = Column(String, nullable=False)

  picture_url = Column(String, nullable=True)
  google_id = Column(String, nullable=True)

  created_at = Column(DateTime, nullable=False, default=datetime.now)
  updated_at = Column(
    DateTime,
    nullable=False,
    onupdate=datetime.now,
    default=datetime.now)
