from pydantic import BaseModel
from datetime import datetime
import uuid as uuid

class UserOut(BaseModel):
  id: uuid.UUID
  email: str
  first_name: str
  last_name: str
  display_name: str
  picture_url: str

  updated_at: datetime
  created_at: datetime

  class Config:
    orm_mode = True
