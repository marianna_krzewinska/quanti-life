from sqlalchemy.orm import Session
from fastapi import status
import uuid as uuid
import httpx

from . import models


async def add_user_to_db(
  db: Session,
  email: str,
  first_name: str,
  last_name: str,
  display_name: str,
  g_id: str,
  picture_url: str):

  db_entry = models.User(
    email = email,
    first_name = first_name,
    last_name = last_name,
    display_name = display_name,
    google_id = g_id,
    picture_url = picture_url,
  )

  db.add(db_entry)
  db.commit()
  db.refresh(db_entry)

  return db_entry


async def get_user_data_from_google(token):
  async with httpx.AsyncClient() as client:
    response = await client.get(
      'https://www.googleapis.com/oauth2/v3/userinfo',
      params = { 'alt': 'json' },
      headers = { 'Authorization': 'Bearer %s' % token }
    )

    if (response.status_code == status.HTTP_200_OK):
      return response.json()

    else:
      raise HTTPException(
        status_code = status.HTTP_400_BAD_REQUEST,
        detail = "Getting user data failed")


async def get_user_by_g_id(db: Session, g_id: str):
  return db.query(models.User) \
    .filter(models.User.google_id == g_id) \
    .first()


async def create_user(db: Session, tokeninfo, token):
  google_user = await get_user_data_from_google(token)

  new_user = await add_user_to_db(
    db,
    google_user['email'],
    google_user['given_name'],
    google_user['family_name'],
    google_user['name'],
    google_user['sub'],
    google_user['picture'])

  return new_user
