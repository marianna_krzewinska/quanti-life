import os
import logging


SERVER_PORT = int(os.getenv('SERVER_PORT', 8000))


POSTGRES_USER = os.getenv('POSTGRES_USER', 'postgres')
POSTGRES_PASS = os.getenv('POSTGRES_PASSWORD', 'postgres')
POSTGRES_HOST = os.getenv('POSTGRES_HOST', 'postgres')
POSTGRES_PORT = os.getenv('POSTGRES_PORT', '5432')
POSTGRES_DB = os.getenv('POSTGRES_DB', 'tracker_db')


SQL_LOG_LEVEL = logging.INFO \
  if (os.getenv('SERVER_SQL_LOG_LEVEL', 'Info') == 'Info') \
  else logging.WARNING


GOOGLE_CLIENT_ID = os.getenv('GOOGLE_CLIENT_ID', 'none')

ROOT_PATH = os.getenv('SERVER_ROOT_PATH', '')
