from fastapi import FastAPI, Request, HTTPException, Header, status, Depends
import httpx
from sqlalchemy.orm import Session

import config as config
from users import methods as user_methods
from database import SessionLocal, engine, get_db

forbidden_exception = HTTPException(
  status_code = status.HTTP_403_FORBIDDEN,
  detail = "Forbidden")


async def authorize(
  authorization: str = Header(...),
  db: Session = Depends(get_db)):

  if (authorization == None or len(authorization) == 0):
    raise forbidden_exception

  async with httpx.AsyncClient() as client:
    auth_response = await client.get(
      'https://www.googleapis.com/oauth2/v3/tokeninfo',
      headers = { 'Authorization': 'Bearer %s' % authorization }
    )

    if (auth_response.status_code == status.HTTP_200_OK):
      tokeninfo = auth_response.json()

      if (tokeninfo['aud'] == config.GOOGLE_CLIENT_ID):
        user = await user_methods.get_user_by_g_id(db, tokeninfo['sub'])

        return { 'user': user, 'tokeninfo': tokeninfo }

    raise forbidden_exception
