import uvicorn
from fastapi import FastAPI

import config as config
from users.router import router as users_router
from entries.router import router as entries_router
from entries.water.router import router as water_entries_router
from entries.urine.router import router as urine_entries_router


app = FastAPI(root_path = config.ROOT_PATH)


app.include_router(users_router)
app.include_router(entries_router)
app.include_router(water_entries_router)
app.include_router(urine_entries_router)


uvicorn.run(app, host = '0.0.0.0' , port = config.SERVER_PORT)
