from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import logging

import config as config


SQLALCHEMY_DATABASE_URL = 'postgresql://%s:%s@%s:%s/%s' % (
  config.POSTGRES_USER,
  config.POSTGRES_PASS,
  config.POSTGRES_HOST,
  config.POSTGRES_PORT,
  config.POSTGRES_DB,
)


engine = create_engine(
  SQLALCHEMY_DATABASE_URL
)


logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(config.SQL_LOG_LEVEL)


SessionLocal = sessionmaker(autocommit = False, autoflush = False, bind = engine)


Base = declarative_base()


# Dependency for controllers
def get_db():
  db = SessionLocal()
  try:
    yield db
  finally:
    db.close()
