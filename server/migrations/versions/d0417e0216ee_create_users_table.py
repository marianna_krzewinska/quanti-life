"""Create users table

Revision ID: d0417e0216ee
Revises: e92d06be6664
Create Date: 2021-03-16 16:48:31.631866

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID


# revision identifiers, used by Alembic.
revision = 'd0417e0216ee'
down_revision = 'e92d06be6664'
branch_labels = None
depends_on = None


def upgrade():
  op.create_table(
    'users',
    sa.Column('id', UUID(as_uuid=True), primary_key=True),

    sa.Column('email', sa.String, nullable=False),
    sa.Column('first_name', sa.String, nullable=False),
    sa.Column('last_name', sa.String, nullable=False),
    sa.Column('display_name', sa.String, nullable=False),

    sa.Column('google_id', sa.String, nullable=True),
    sa.Column('picture_url', sa.Text, nullable=True),

    sa.Column('created_at', sa.DateTime, nullable=False),
    sa.Column('updated_at', sa.DateTime, nullable=False),
  )


def downgrade():
  op.drop_table('users')
