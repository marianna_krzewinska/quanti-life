"""create urine entries table

Revision ID: e92d06be6664
Revises: 68e0ceffb9ec
Create Date: 2021-03-11 22:02:47.182269

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = 'e92d06be6664'
down_revision = '68e0ceffb9ec'
branch_labels = None
depends_on = None


def upgrade():
  op.create_table(
    'urine_entries',
    sa.Column('id', UUID(as_uuid=True), primary_key=True),
    sa.Column('volume', sa.Integer, nullable=False),
    sa.Column('color', sa.String, nullable=False),
    sa.Column('user_id', UUID(as_uuid=True), nullable=True),
    sa.Column('timestamp', sa.DateTime, nullable=False),
    sa.Column('created_at', sa.DateTime, nullable=False),
    sa.Column('updated_at', sa.DateTime, nullable=False),
  )



def downgrade():
  op.drop_table('urine_entries')
