"""create water_entries table

Revision ID: 68e0ceffb9ec
Revises:
Create Date: 2021-03-11 13:37:35.829230

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = '68e0ceffb9ec'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
  op.create_table(
    'water_entries',
    sa.Column('id', UUID(as_uuid=True), primary_key=True),
    sa.Column('volume', sa.Integer, nullable=False),
    sa.Column('user_id', UUID(as_uuid=True), nullable=True),
    sa.Column('timestamp', sa.DateTime, nullable=False),
    sa.Column('created_at', sa.DateTime, nullable=False),
    sa.Column('updated_at', sa.DateTime, nullable=False),
  )


def downgrade():
  op.drop_table('water_entries')
