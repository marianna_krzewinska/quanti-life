# Flutter dockerized app setup

## Local development with Code Server

```bash
[~/d/flutter-docker] : docker-compose up --build
```

Code Server runs by default on port 8080 (see [running Code Server on another port](#id-like-to-start-codeserver-on-another-port)). There you can install Dart & Flutter extensions and start debug - press F5 and select device on which to run the app. It can also ask you to select what type of debugging to run - in that case, select Dart and then your app name.

## VSC remote development

### Additional requirements

- Visual Studio Code

### Starting app

1. Install Remote Development, Dart and Flutter extensions.
2. Start remote development - run command: `Remote-Containers: Open Folder in Container` and select your project folder (top one, containing folder .devcontainer).
   1. If you're using OS other than Linux, it might necessary to set another usb path in `devcontainer.json` file
3. Once the VSCode opens a new window with Dev Container, press F5 and select device on which to run the app. It can also ask you to select what type of debugging to run - in that case, select Dart and then your app name.

## Other IDE development with Flutter run in docker container

This requires a little more effort to run the application and there's no support for debugging. It's an option for people who want to dockerize Flutter - to ensure the same development environment for all developers involved, but their IDE of choice doesn't support debugging of dockerized Flutter.

If there's an IDE that supports dockerized Flutter debugging, then please let me know - I'd like to make this a comprehensive dockerized Flutter development guide.

Create file `docker-compose.override.yaml`

```yml
version: "3.3"
services:
  mobile:
    stdin_open: true
    working_dir: /home/developer/workspace/workspace/[app folder]
    command: sh -c "flutter pub get && flutter run"
```

### Testing connection with the phone

```bash
# Make sure to connect your phone and run:

[~/d/flutter-docker] : docker run -it --rm --privileged -v '/dev/bus/usb:/dev/bus/usb' flutter-docker_mobile flutter doctor

# You should see something like that if phone is correctly connected and all permissions granted:

Doctor summary (to see all details, run flutter doctor -v):
[✓] Flutter (Channel master, 1.26.0-2.0.pre.296, on Linux, locale en_US)
[✓] Android toolchain - develop for Android devices (Android SDK version 29.0.2)
[✗] Chrome - develop for the web (Cannot find Chrome executable at google-chrome)
    ! Cannot find Chrome. Try setting CHROME_EXECUTABLE to a Chrome executable.
[!] Android Studio (not installed)
[✓] Connected device (1 available)

! Doctor found issues in 2 categories.
```

### 1. Running the app

```bash
[~/d/flutter-docker] : docker-compose up --build

# The app should start on your phone in debug mode
```

### 2. Flutter hot reload

To hot reload you have to attach externally to the Flutter container

```bash
[~/d/flutter-docker] : docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED         STATUS         PORTS     NAMES
efc2fe73cf8c   flutter-docker_mobile   "sh -c 'rm -rf build…"   6 minutes ago   Up 6 minutes             flutter-docker_mobile_1

[~/d/flutter-docker] : docker attach flutter-docker_mobile_1
```

## Common Issues

### Error: Method not found: 'Vector3'. Error: 'Matrix4' isn't a type. Error: Getter not found: 'Matrix4'.

If you get an error containing any of these, there's a big chance that it's caused by lack of some libraries: `Error: Method not found: 'Vector3'. Error: 'Matrix4' isn't a type. Error: Getter not found: 'Matrix4'.`

* **Solution**: run `flutter pub get` in your Flutter app folder (e.g. workspace/workspace/myapp when running in Code Server) before starting the app. Flutter sometimes downloads them, and sometimes doesn't, so it's best to do it at least before the first run. You can notice that this command is executed before each app run or build command.

### I'd like to start Code Server on another port

Add ports override to `docker-compose.override.yaml`. E.g. starting Code Server on port 9000.

```yml
version: "3.3"
services:
  mobile:
    ports:
      - "9000:8080"
```



## Useful links

1. [Tooltips on charts](https://github.com/google/charts/issues/58)
