import 'package:redux_epics/redux_epics.dart';

import '../main_state.dart';
import '../urine/urine_state.dart';
import '../utils.dart';
import '../water/water_state.dart';
import 'summary_model.dart';


final List summaryEpics = [
  selectSummaryDateEpic,
  selectSummaryPeriodEpic,
];


SummarySettings summaryReducer(SummarySettings state, SummaryAction action) {
  switch (action.type) {
    case SummaryActions.SelectDate:
      return state.update(newSelectedDay: action.selectedDate);

    case SummaryActions.SelectPeriod:
      return state.update(
        newSelectedPeriod: SelectedPeriod(action.periodStart, action.periodEnd));

    default:
      break;
  }

  return state;
}


Stream<dynamic> selectSummaryDateEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == SummaryActions.SelectDate)
    .expand((action) {
      DateTime todayMidnight = justDate(action.selectedDate);
      DateTime todayEvening = todayMidnight.add(Duration(
        hours: 23,
        minutes: 59,
        seconds: 59));

      return [
        GetWaterEntriesSummaryAction(
          todayMidnight,
          todayEvening,
        ),
        GetUrineEntriesSummaryAction(
          todayMidnight,
          todayEvening,
        ),
      ];
    });
}


Stream<dynamic> selectSummaryPeriodEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == SummaryActions.SelectPeriod)
    .expand((action) {
      return [
        GetWaterEntriesSummaryAction(
          action.periodStart,
          action.periodEnd,
        ),
        GetUrineEntriesSummaryAction(
          action.periodStart,
          action.periodEnd,
        ),
      ];
    });
}


abstract class SummaryAction {
  DateTime selectedDate;
  DateTime periodStart;
  DateTime periodEnd;

  SummaryActions type;
}


class SelectSummaryDateAction extends SummaryAction {
  SummaryActions type = SummaryActions.SelectDate;
  DateTime selectedDate;

  SelectSummaryDateAction(this.selectedDate);
}


class SelectSummaryPeriodAction extends SummaryAction {
  SummaryActions type = SummaryActions.SelectPeriod;
  DateTime periodStart;
  DateTime periodEnd;

  SelectSummaryPeriodAction(this.periodStart, this.periodEnd);
}


enum SummaryActions {
  SelectDate,
  SelectPeriod,
}
