import '../urine/urine_model.dart';
import '../water/water_model.dart';


class SelectedPeriod {
  DateTime start;
  DateTime end;

  SelectedPeriod(this.start, this.end);

  static initial() => SelectedPeriod(DateTime.now(), DateTime.now());
}


class SummarySettings {
  DateTime selectedDay;
  SelectedPeriod selectedPeriod;

  SummarySettings(this.selectedDay, this.selectedPeriod);

  SummarySettings update({ newSelectedDay, newSelectedPeriod }) {
    return SummarySettings(
      newSelectedDay ?? selectedDay,
      newSelectedPeriod ?? selectedPeriod,
    );
  }

  static initial() => SummarySettings(DateTime.now(), SelectedPeriod.initial());
}


class SummaryData {
  WaterSummary waterSummary;
  UrineSummary urineSummary;
  SelectedPeriod selectedPeriod;
  DateTime selectedDate;

  SummaryData(
    this.waterSummary,
    this.urineSummary,
    this.selectedPeriod,
    this.selectedDate);
}
