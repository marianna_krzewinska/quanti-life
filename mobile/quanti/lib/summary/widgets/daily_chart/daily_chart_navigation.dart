import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

import '../../../main_state.dart';
import '../../../utils.dart';
import '../../summary_state.dart';


class DailyChartNavigation extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final store = StoreProvider.of<AppState>(context);

    return StoreConnector<AppState, DateTime>(
      converter: (store) => store.state.summarySettings.selectedDay,

      builder: (BuildContext context, DateTime selectedDate) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
            icon: Icon(
              Icons.keyboard_arrow_left,
              size: 40),
            onPressed: () {
              store.dispatch(SelectSummaryDateAction(
                selectedDate.subtract(Duration(days: 1)),
              ));
            }),

          Text(
            formatDate(selectedDate),
            style: TextStyle(
              fontWeight: FontWeight.w300,
              fontSize: 20,
              height: 1.7
            )),

          IconButton(
            icon: Icon(
              Icons.keyboard_arrow_right,
              color: _forwardButtonEnabled(selectedDate)
                ? Colors.white
                : Colors.white.withOpacity(0.4),
              size: 40),
            onPressed: () {
              if (!_forwardButtonEnabled(selectedDate)) { return; }

              store.dispatch(SelectSummaryDateAction(
                selectedDate.add(Duration(days: 1)),
              ));
            })
        ],
      ),
    );
  }

  bool _forwardButtonEnabled(DateTime selectedDate) {
    DateTime today = justDate(DateTime.now());

    return selectedDate.isBefore(today) && !selectedDate.isAtSameMomentAs(today);
  }
}
