import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

import '../../../configs/base.dart';
import '../../../main_state.dart';
import '../../../urine/urine_model.dart';
import '../../../urine/urine_utils.dart';
import '../../../utils.dart';
import '../../../water/water_model.dart';
import '../../summary_model.dart';
import '../../summary_state.dart';
import 'daily_chart_navigation.dart';


class DailyChart extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final store = StoreProvider.of<AppState>(context);

    store.dispatch(SelectSummaryDateAction(DateTime.now()));

    return StoreConnector<AppState, SummaryData>(
      converter: (store) => SummaryData(
        store.state.waterEntries.summary,
        store.state.urineEntries.summary,
        null,
        store.state.summarySettings.selectedDay,
      ),
      builder: (BuildContext context, SummaryData summary) => Card(
        shape: ContinuousRectangleBorder(borderRadius: BorderRadius.zero),
        child: Column(
          children: [
            Container(height: 10),
            DailyChartNavigation(),
            Container(
              padding: EdgeInsets.only(
                left: 10,
                right: 10
              ),
              height: 450,
              child: charts.TimeSeriesChart(
                [
                  _getBaselineSeries(summary.selectedDate),
                  if (summary.waterSummary.aggregates.isNotEmpty)
                    _getWaterSummarySeries(summary.waterSummary),
                  if (summary.urineSummary.aggregates.isNotEmpty)
                    ..._getUrineSummarySeries(summary.urineSummary),
                ],
                defaultRenderer: _chartRendrer,
                domainAxis: charts.DateTimeAxisSpec(
                  showAxisLine: false,
                  tickFormatterSpec: _timeTickFormatterSpec,
                  tickProviderSpec: charts.StaticDateTimeTickProviderSpec(
                    _getTimeAxisTics(summary.selectedDate)
                  ),
                  renderSpec: charts.SmallTickRendererSpec(
                    labelStyle: charts.TextStyleSpec(
                      fontSize: 10, color: charts.MaterialPalette.white),
                  ),
                ),
                primaryMeasureAxis: charts.NumericAxisSpec(
                  renderSpec: charts.SmallTickRendererSpec(
                    labelStyle: charts.TextStyleSpec(
                      fontSize: 10, color: charts.MaterialPalette.white),
                  ),
                  showAxisLine: false,
                  tickProviderSpec: charts.StaticNumericTickProviderSpec(
                    _volumeAxisTics,
                  ),
                  tickFormatterSpec: _volumeTickFormatterSpec,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }


  charts.Series<WaterAggregate, DateTime> _getWaterSummarySeries(WaterSummary summary) {
    return charts.Series<WaterAggregate, DateTime>(
      id: 'Water',
      domainFn: (WaterAggregate entry, _) => entry.timestamp,
      measureFn: (WaterAggregate entry, _) => entry.volume,
      data: summary.aggregates,
      seriesColor: charts.Color.fromHex(code: ColorMap.waterHex),
    );
  }

  List<charts.Series<UrineAggregate, DateTime>> _getUrineSummarySeries(
    UrineSummary summary) {

    return summary.aggregates
      .map((aggregates) {
        charts.Color color = charts.ColorUtil
          .fromDartColor(urineColors
            .firstWhere((color) => color.id == aggregates.first.colorId)
            .color);

        return charts.Series<UrineAggregate, DateTime>(
          id: 'Water',
          domainFn: (UrineAggregate entry, _) => entry.timestamp,
          measureFn: (UrineAggregate entry, _) => entry.volume * -1,
          data: aggregates,
          seriesColor: color,
        );
      })
      .toList();
  }

  charts.Series<DateTime, DateTime> _getBaselineSeries(DateTime day) =>
    charts.Series<DateTime, DateTime>(
      id: 'Baseline',
      domainFn: (point, _) => point,
      measureFn: (point, _) => 0,
      data: List.generate(23, (index) => justDate(day).add(Duration(hours: index + 1))),
      seriesColor: charts.MaterialPalette.transparent,
    );

  List<charts.TickSpec<DateTime>> _getTimeAxisTics(DateTime date) {
    DateTime midnight = justDate(date);

    return [0, 4, 8, 12, 16, 20]
      .map((int hour) => charts.TickSpec(midnight.add(Duration(hours: hour))))
      .toList();
  }

  final List<charts.TickSpec<int>> _volumeAxisTics = [800, 400, 0, -400, -800]
    .map((int volume) => charts.TickSpec(volume))
    .toList();

  final _volumeTickFormatterSpec = charts.BasicNumericTickFormatterSpec(_absoluteVolumes);
  final _timeTickFormatterSpec = charts.AutoDateTimeTickFormatterSpec(
    hour: charts.TimeFormatterSpec(format: 'HH', transitionFormat: 'HH'),
  );

  final charts.SeriesRendererConfig<DateTime> _chartRendrer = charts.BarRendererConfig(
    groupingType: charts.BarGroupingType.stacked,
  );
}

// Defined outside chart so that it can be used in an initializer
String _absoluteVolumes(value) => (value > 0 ? value : value * -1)
  .toInt()
  .toString();
