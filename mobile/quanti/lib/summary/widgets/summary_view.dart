import 'package:flutter/material.dart';

import './daily_chart/daily_chart.dart';
import './weekly_chart/weekly_chart.dart';


class SummaryView extends StatelessWidget {
  static String route = '/summary';

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(text: 'DAY'),
              Tab(text: 'WEEK'),
            ],
          ),
          centerTitle: true,
          title: Text('Summary'),
        ),
        body: TabBarView(
          children: [
            DailyChart(),
            WeeklyChart(),
          ],
        ),
      ),
    );
  }
}
