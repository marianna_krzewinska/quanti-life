import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

import '../../../configs/base.dart';
import '../../../main_state.dart';
import '../../../urine/urine_model.dart';
import '../../../urine/urine_utils.dart';
import '../../../utils.dart';
import '../../../water/water_model.dart';
import '../../summary_model.dart';
import '../../summary_state.dart';
import 'weekly_chart_navigation.dart';


class WeeklyChart extends StatelessWidget {

  final _waterRendererId = 'waterLine';

  @override
  Widget build(BuildContext context) {

    final store = StoreProvider.of<AppState>(context);

    DateTime today = justDate(DateTime.now());

    store.dispatch(SelectSummaryPeriodAction(
      today.subtract(Duration(days: today.weekday - 1)),
      today.add(Duration(days: DateTime.daysPerWeek - today.weekday)),
    ));

    return StoreConnector<AppState, SummaryData>(
      converter: (store) => SummaryData(
        store.state.waterEntries.summary,
        store.state.urineEntries.summary,
        store.state.summarySettings.selectedPeriod,
        null,
      ),
      builder: (BuildContext context, SummaryData summary) => Card(
        shape: ContinuousRectangleBorder(borderRadius: BorderRadius.zero),
        child: Column(
          children: [
            Container(height: 10),
            WeeklyChartNavigation(),
            Container(
              padding: EdgeInsets.only(
                left: 10,
                right: 10
              ),
              height: 450,
              child: charts.OrdinalComboChart(
                _getSeries(summary),
                defaultRenderer: new charts.BarRendererConfig(
                  groupingType: charts.BarGroupingType.stacked,
                ),
                customSeriesRenderers: [
                  new charts.LineRendererConfig(
                    includePoints: true,
                    customRendererId: _waterRendererId),
                ],
                domainAxis: charts.OrdinalAxisSpec(
                  showAxisLine: false,
                  renderSpec: charts.SmallTickRendererSpec(
                    labelStyle: charts.TextStyleSpec(
                      fontSize: 10, color: charts.MaterialPalette.white),
                  )
                ),
                primaryMeasureAxis: charts.NumericAxisSpec(
                  renderSpec: charts.SmallTickRendererSpec(
                    labelStyle: charts.TextStyleSpec(
                      fontSize: 10, color: charts.MaterialPalette.white),
                  ),
                  showAxisLine: false,
                  tickProviderSpec: charts.StaticNumericTickProviderSpec(
                    _getVolumeAxisTics(),
                  ),
                ),
              )
            )
          ]
        ),
      ),
    );
  }

  List<charts.Series<dynamic, String>> _getSeries(SummaryData summary) {
    return [
      _getBaselineSeries(summary.selectedPeriod),
      _getWaterSummarySeries(summary.waterSummary),
      ..._getUrineSummarySeries(summary.urineSummary)
    ];
  }

  charts.Series<num, String> _getBaselineSeries(SelectedPeriod selectedPeriod) =>
    charts.Series<num, String>(
      id: 'Baseline',
      domainFn: (num day, _) => day.toString(),
      measureFn: (num day, _) => 0,
      data: List.generate(7, (index) => selectedPeriod
        .start
        .add(Duration(days: index))
        .day),
      seriesColor: charts.MaterialPalette.transparent,
    );

  charts.Series<WaterAggregate, String> _getWaterSummarySeries(WaterSummary summary) {
    return charts.Series<WaterAggregate, String>(
      id: 'Water',
      domainFn: (WaterAggregate entry, _) => justDate(entry.timestamp).day.toString(),
      measureFn: (WaterAggregate entry, _) => entry.volume,
      data: summary.aggregates,
      seriesColor: charts.Color.fromHex(code: ColorMap.waterHex),
    )..setAttribute(charts.rendererIdKey, _waterRendererId);
  }

  List<charts.Series<UrineAggregate, String>> _getUrineSummarySeries(UrineSummary summary) {
    return summary.aggregates
      .map((aggregates) {
        charts.Color color = charts.ColorUtil
          .fromDartColor(urineColors
            .firstWhere((color) => color.id == aggregates.first.colorId)
            .color);

        return charts.Series<UrineAggregate, String>(
          id: 'Urine ${color.hexString}',
          domainFn: (UrineAggregate entry, _) => justDate(entry.timestamp)
            .day
            .toString(),
          measureFn: (UrineAggregate entry, _) => entry.volume,
          data: aggregates,
          seriesColor: color,
        );
      })
      .toList();
  }

  List<charts.TickSpec<int>> _getVolumeAxisTics() => [2000, 1600, 1200, 800, 400, 0]
    .map((int volume) => charts.TickSpec(volume))
    .toList();
}
