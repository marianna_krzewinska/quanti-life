import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../main_state.dart';
import '../../../utils.dart';
import '../../summary_model.dart';
import '../../summary_state.dart';


class WeeklyChartNavigation extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final store = StoreProvider.of<AppState>(context);

    return StoreConnector<AppState, SelectedPeriod>(
      converter: (store) => store.state.summarySettings.selectedPeriod,

      builder: (BuildContext context, SelectedPeriod selectedPeriod) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
            icon: Icon(
              Icons.keyboard_arrow_left,
              size: 40),
            onPressed: () {
              store.dispatch(SelectSummaryPeriodAction(
                selectedPeriod.start.subtract(Duration(days: 7)),
                selectedPeriod.end.subtract(Duration(days: 7)),
              ));
            }),

          Text(
            formatDateRange(selectedPeriod),
            style: TextStyle(
              fontWeight: FontWeight.w300,
              fontSize: 20,
              height: 1.7
            )),

          IconButton(
            icon: Icon(
              Icons.keyboard_arrow_right,
              color: _forwardButtonEnabled(selectedPeriod)
                ? Colors.white
                : Colors.white.withOpacity(0.4),
              size: 40),
            onPressed: () {
              if (!_forwardButtonEnabled(selectedPeriod)) { return; }

              store.dispatch(SelectSummaryPeriodAction(
                selectedPeriod.start.add(Duration(days: 7)),
                selectedPeriod.end.add(Duration(days: 7)),
              ));
            })
        ],
      ),
    );
  }

  bool _forwardButtonEnabled(SelectedPeriod selectedPeriod) {
    DateTime today = justDate(DateTime.now());

    return selectedPeriod.end.isBefore(today)
      && !selectedPeriod.end.isAtSameMomentAs(today);
  }
}


String formatDateRange(SelectedPeriod selectedPeriod) {
  DateTime start = selectedPeriod.start;
  DateTime end = selectedPeriod.end;

  if (start.month == end.month) {
    return '${DateFormat('MMM').format(start)} ${start.day} - ${end.day}';
  }

  return '${DateFormat('MMM').format(start)} ${start.day} - ' +
    '${DateFormat('MMM').format(end)} ${end.day}';
}
