import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

import '../entries/entries_view.dart';
import '../main_state.dart';
import '../summary/widgets/summary_view.dart';
import '../urine/widgets/urine_main_tile.dart';
import '../user/user_settings_view.dart';
import '../water/widgets/water_main_tile.dart';


class HomeView extends StatelessWidget {
  static String route = '/home';

  @override
  Widget build(BuildContext context) {

    return StoreConnector<AppState, Function>(
      converter: (store) =>
        (String location) => store.dispatch(NavigateToAction.push(location)),
      rebuildOnChange: false,
      builder: (BuildContext context, navCallback) => Scaffold(
        appBar: AppBar(
          title: Text('Today'),
          actions: [
            IconButton(
              icon: Icon(Icons.bar_chart),
              onPressed: () => navCallback(SummaryView.route),
            ),
            IconButton(
              icon: Icon(Icons.list),
              onPressed: () => navCallback(EntriesView.route),
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () => navCallback(UserSettingsView.route),
            ),
          ],
        ),

        body: ListView(
          children: [
            WaterMainTile(),
            UrineMainTile(),
          ],
        ),
      ),
    );
  }
}
