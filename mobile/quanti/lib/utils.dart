import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


bool isTest = Platform.environment.containsKey('FLUTTER_TEST');


String getHour(DateTime date) => formatTime(TimeOfDay.fromDateTime(date));


String formatTime(TimeOfDay time) => '${time.hour.toString().padLeft(2, '0')}:'
  + '${time.minute.toString().padLeft(2, '0')}';


String formatDate(DateTime date) => DateFormat('EEE, MMM d').format(date);


DateTime justDate(DateTime date) => DateTime(date.year, date.month, date.day);
