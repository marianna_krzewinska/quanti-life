import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux/redux.dart';

import 'auth.dart';
import 'configs/base.dart';
import 'configs/index.dart';
import 'entries/entries_view.dart';
import 'home/home_view.dart';
import 'login_view.dart';
import 'main_state.dart';
import 'summary/widgets/summary_view.dart';
import 'user/user_settings_view.dart';
import 'shared_preferences.dart';


void main(AppConfig config) async {
  WidgetsFlutterBinding.ensureInitialized();
  await initializeSharedPreferences();

  initializeConfig(config);

  final epicMiddleware = EpicMiddleware<AppState>(epics);

  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
    middleware: [
      NavigationMiddleware<AppState>(),
      epicMiddleware
    ],
  );

  runApp(App(store: store));
}


class App extends StatelessWidget {
  final Store<AppState> store;

  App({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Quanti.life',
        theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: ColorMap.bg,
          secondaryHeaderColor: Colors.white,
          accentColor: Colors.white,
        ),
        initialRoute: AuthView.route,
        navigatorKey: NavigatorHolder.navigatorKey,
        routes: {
          HomeView.route: (BuildContext context) => HomeView(),
          EntriesView.route: (BuildContext context) => EntriesView(),
          LoginView.route: (BuildContext context) => LoginView(),
          AuthView.route: (BuildContext context) => AuthView(),
          SummaryView.route: (BuildContext context) => SummaryView(),
          UserSettingsView.route: (BuildContext context) => UserSettingsView(),
        },
      ));
  }
}
