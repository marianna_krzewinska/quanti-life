import 'package:flutter/cupertino.dart';


class UrineIcons {
  static const IconData toilet = IconData(0xf7d8, fontFamily: 'DropletFont');
  static const IconData threeDrops = IconData(0xe800, fontFamily: 'DropletFont');
  static const IconData glossyDrop1 = IconData(0xe801, fontFamily: 'DropletFont');
  static const IconData glossyDrop2 = IconData(0xe9a9, fontFamily: 'DropletFont');
  static const IconData singleDrop = IconData(0xeae4, fontFamily: 'DropletFont');

  UrineIcons();
}


class UrineColor {
  int id;
  Color color;
  String code;

  UrineColor(this.id, this.color, this.code);
}


List<UrineColor> urineColors = [
  UrineColor(0, Color.fromARGB(255, 255, 249, 196), '255,255,249,196'),
  UrineColor(1, Color.fromARGB(255, 255, 241, 118), '255,255,241,118'),
  UrineColor(2, Color.fromARGB(255, 255, 235, 59), '255,255,235,59'),
  UrineColor(3, Color.fromARGB(255, 253, 216, 53), '255,253,216,53'),
  UrineColor(4, Color.fromARGB(255, 251, 192, 45), '255,251,192,45'),
  UrineColor(5, Color.fromARGB(255, 249, 168, 37), '255,249,168,37'),
];


int getUrineColorIdByCode(String code) => urineColors
  .singleWhere((color) => color.code == code)
  .id;


String getUrineColorCodeById(int id) => urineColors
  .singleWhere((color) => color.id == id)
  .code;
