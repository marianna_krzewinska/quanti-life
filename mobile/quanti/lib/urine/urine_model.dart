import 'dart:convert';

import '../entries/entries_model.dart';
import 'urine_utils.dart';


UrineColor defaultUrineColor = urineColors[0];


class UrineEntriesState {
  List<UrineEntry> list;
  UrineSummary summary;

  UrineEntriesState(this.list, this.summary);

  static UrineEntriesState initial() {
    return UrineEntriesState([], UrineSummary.initial());
  }

  UrineEntriesState update({
    List<UrineEntry> newList,
    UrineSummary newSummary,
  }) {
    return UrineEntriesState(
      newList ?? list,
      newSummary ?? summary);
  }
}


class UrineAggregate {
  int volume;
  DateTime timestamp;
  int colorId;

  UrineAggregate(this.volume, this.timestamp, this.colorId);

  static List<List<UrineAggregate>> parseJSON(List data) {
    Map<int, List<UrineAggregate>> groupedByColor = {
      urineColors[0].id: [],
      urineColors[1].id: [],
      urineColors[2].id: [],
      urineColors[3].id: [],
      urineColors[4].id: [],
      urineColors[5].id: [],
    };

    data.forEach((dynamic entry) {
      int colorId = getUrineColorIdByCode(entry['color']);

      groupedByColor[colorId].add(UrineAggregate(
        entry['volume'],
        DateTime.parse(entry['timestamp']),
        colorId));
    });

    return groupedByColor
      .values
      .where((element) => element.isNotEmpty)
      .toList();
  }
}


class UrineSummary {
  DateTime start;
  DateTime end;
  List<List<UrineAggregate>> aggregates;

  UrineSummary({ this.start, this.end, this.aggregates });

  static initial() => UrineSummary(
    start: null,
    end: null,
    aggregates: []);

  static UrineSummary fromJSON(dynamic data) => data is UrineSummary
    ? data
    : UrineSummary(
      start: DateTime.parse(data['start']),
      end: DateTime.parse(data['end']),
      aggregates: UrineAggregate.parseJSON(data['aggregates']),
    );
}


class UrineEntry extends Entry {
  String id;
  int volume;
  int colorId;
  DateTime timestamp;
  DateTime updatedAt;
  DateTime createdAt;
  EntryType type = EntryType.EXCRETION_URINE;

  UrineEntry(
    this.id,
    this.volume,
    this.colorId,
    this.timestamp,
    this.updatedAt,
    this.createdAt): super(timestamp);

  static UrineEntry create(volume) => UrineEntry(
    '',
    volume,
    defaultUrineColor.id,
    DateTime.now(),
    DateTime.now(),
    DateTime.now());

  static UrineEntry fromJSON(entry) => entry is UrineEntry
    ? entry
    : UrineEntry(
      entry['id'],
      entry['volume'],
      getUrineColorIdByCode(entry['color']),
      DateTime.parse(entry['timestamp']),
      DateTime.parse(entry['updated_at']),
      DateTime.parse(entry['created_at']),
    );

  UrineEntry update({
    int newVolume,
    int newColorId,
    DateTime newTimestamp}) => UrineEntry(
      id,
      newVolume ?? volume,
      newColorId ?? colorId,
      newTimestamp ?? timestamp,
      DateTime.now(),
      createdAt);

  String toJSON() => jsonEncode({
    'volume': volume,
    'timestamp': timestamp.toIso8601String(),
    'color': getUrineColorCodeById(colorId),
  });
}
