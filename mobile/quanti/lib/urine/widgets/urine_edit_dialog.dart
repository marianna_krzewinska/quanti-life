import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

import '../../main_state.dart';
import '../../utils.dart';
import '../urine_model.dart';
import '../urine_state.dart';
import '../urine_utils.dart';


class UrineEditDialog extends StatefulWidget {
  final UrineEntry urineEntry;
  final bool createNew;

  UrineEditDialog({Key key, this.urineEntry, this.createNew}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _UrineEditDialogState();
}


class _UrineEditDialogState extends State<UrineEditDialog> {
  int _selectedVolume;
  DateTime _selectedDate;
  TimeOfDay _selectedTime;
  UrineColor _selectedColor;

  @override
  void initState() {
    super.initState();

    _selectedColor = urineColors
      .singleWhere((color) => color.id == widget.urineEntry.colorId);
    _selectedVolume = widget.urineEntry.volume;
    _selectedTime = TimeOfDay(
      hour: widget.urineEntry.timestamp.hour,
      minute: widget.urineEntry.timestamp.minute);
    _selectedDate = DateTime(
      widget.urineEntry.timestamp.year,
      widget.urineEntry.timestamp.month,
      widget.urineEntry.timestamp.day,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
      children: [
        Row(
          children: [
            GestureDetector(
              child: Text(
                formatDate(_selectedDate),
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w300
                ),
              ),
              onTap: () => _handleChangeDate(context),
            ),

            GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(left: 20),
                child: Text(
                  formatTime(_selectedTime),
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w300
                  ),
                ),
              ),
              onTap: () => _handleChangeTime(context),
            ),
          ]),

        Padding(
          padding: EdgeInsets.only(top: 40),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: IconButton(
                  icon: Icon(UrineIcons.toilet, color: _getIconColor(100)),
                  iconSize: 30,
                  onPressed: _getVolumeChangeCallback(100),
                ),
              ),

              Padding(
                padding: EdgeInsets.only(left: 5),
                child: IconButton(
                  icon: Icon(UrineIcons.toilet, color: _getIconColor(300)),
                  iconSize: 40,
                  onPressed: _getVolumeChangeCallback(300),
                ),
              ),

              IconButton(
                icon: Icon(UrineIcons.toilet, color: _getIconColor(500)),
                iconSize: 50,
                onPressed: _getVolumeChangeCallback(500),
              ),
            ],
          ),
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 10),
              child: Text(
                '100 ml',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w300,
                  color: _getTextColor(100),
                ),
              ),
            ),

            Text(
              '300 ml',
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w300,
                color: _getTextColor(300),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(right: 10),
              child: Text(
                '500 ml',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w300,
                  color: _getTextColor(100),
                ),
              ),
            ),
          ],
        ),

        Padding(
          padding: EdgeInsets.only(top: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 5),
                child: Text(
                  'Color',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w300
                  ),
                ),
              ),

              Slider(
                value: _selectedColor.id.toDouble(),
                onChanged: _handleColorChange,
                activeColor: _selectedColor.color,
                inactiveColor: _selectedColor.color,
                min: urineColors.first.id.toDouble(),
                max: urineColors.last.id.toDouble(),
                divisions: urineColors.length - 1,
              ),
            ],
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 40),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              if (widget.createNew) StoreConnector<AppState, Function>(
                builder: (BuildContext context, Function handleClose) => IconButton(
                  iconSize: 35,
                  icon: Icon(Icons.close),
                  onPressed: handleClose),
                converter: (store) => () => store.dispatch(NavigateToAction.pop()),
              ),

              if (!widget.createNew) StoreConnector<AppState, Function>(
                builder: (BuildContext context, Function handleDelete) => IconButton(
                  iconSize: 35,
                  icon: Icon(Icons.delete_outline),
                  onPressed: handleDelete),
                converter: (store) => () => _delete(store),
              ),

              StoreConnector<AppState, Function>(
                builder: (BuildContext context, Function handleSave) => IconButton(
                  iconSize: 35,
                  icon: Icon(Icons.done),
                  onPressed: handleSave),
                converter: (store) => () => _save(store),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void _save(store) {
    DateTime timestamp = DateTime(
      _selectedDate.year,
      _selectedDate.month,
      _selectedDate.day,
      _selectedTime.hour,
      _selectedTime.minute,
    );

    UrineEntry updatedEntry = widget.urineEntry.update(
      newVolume: _selectedVolume,
      newColorId: _selectedColor.id,
      newTimestamp: timestamp,
    );

    if (widget.createNew) {
      store.dispatch(CreateUrineEntryAction(updatedEntry));
    } else {
      store.dispatch(UpdateUrineEntryAction(updatedEntry));
    }

    store.dispatch(NavigateToAction.pop());
  }

  void _delete(store) {
    store.dispatch(DeleteUrineEntryAction(widget.urineEntry.id));
    store.dispatch(NavigateToAction.pop());
  }

  void _handleColorChange(double urineColorId) {
    setState(() {
      _selectedColor = urineColors
        .singleWhere((element) => element.id == urineColorId);
    });
  }

  void _handleChangeDate(BuildContext context) async {
    DateTime newDate = await showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: _selectedDate.subtract(Duration(days: 365)),
      lastDate: DateTime.now()
    );

    setState(() {
      _selectedDate = newDate ?? _selectedDate;
    });
  }

  void _handleChangeTime(BuildContext context) async {
    TimeOfDay newTime = await showTimePicker(
      context: context,
      initialTime: _selectedTime,
    );

    setState(() {
      _selectedTime = newTime ?? _selectedTime;
    });
  }

  Color _getIconColor(int buttonVolume) => _selectedVolume == buttonVolume
    ? _selectedColor.color
    : _selectedColor.color.withOpacity(0.4);

  Color _getTextColor(int volume) => _selectedVolume == volume
    ? Colors.white
    : Colors.white.withOpacity(0.4);

  Function _getVolumeChangeCallback(int volume) => () => setState(() {
    _selectedVolume = volume;
  });
}
