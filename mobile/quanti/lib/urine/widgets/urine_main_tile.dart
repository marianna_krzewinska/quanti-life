import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../configs/base.dart';
import '../../main_state.dart';
import '../../utils.dart';
import '../urine_model.dart';
import '../urine_state.dart';
import '../urine_utils.dart';
import 'urine_color_bar.dart';
import 'urine_edit_dialog.dart';


class UrineMainTile extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);
    store.dispatch(GetUrineEntriesAction());

    return Card(
      shape: ContinuousRectangleBorder(borderRadius: BorderRadius.zero),
      margin: EdgeInsets.only(top: 20, left: 15, right: 15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 10,
                child: Container(width: 0, height: 150),
              ),

              StoreConnector<AppState, List<UrineEntry>>(
                builder: (BuildContext context, List<UrineEntry> entries) => Column(
                  children: [
                    Text(
                      getUrineMl(entries),
                      style: TextStyle(
                        color: ColorMap.urine,
                        fontWeight: FontWeight.w300,
                        fontSize: 40,
                      ),
                    ),
                    UrineColorBar(entries: entries),
                  ],
                ),
                converter: (store) => getTodaysEntries(store.state.urineEntries.list),
              ),

              Padding(
                padding: EdgeInsets.only(
                  right: 60,
                  bottom: 20,
                  left: 25),
                child: IconButton(
                  icon: Icon(
                    UrineIcons.toilet,
                    color: ColorMap.urine,
                    size: 50,
                  ),
                  onPressed: () => showDialog(
                    context: context,
                    child: UrineEditDialog(
                      urineEntry: UrineEntry.create(500),
                      createNew: true,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}


String getUrineMl(List<UrineEntry> entries) {
  List<int> mls = entries
    .map((entry) => entry.volume)
    .toList();

  int ml = (mls.isEmpty ? [0] : mls).reduce((value, entry) => value + entry);

  return '$ml ml';
}


List<UrineEntry> getTodaysEntries(List<UrineEntry> entries) {
  DateTime todayMidnight = justDate(DateTime.now());

  return entries
    .where((entry) => entry.timestamp.isAfter(todayMidnight))
    .toList();
}
