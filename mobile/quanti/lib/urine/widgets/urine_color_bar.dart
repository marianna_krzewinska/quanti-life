import 'package:flutter/material.dart';

import '../urine_model.dart';
import '../urine_utils.dart';


const maxWidth = 140;


class BarConfig {
  double width;
  Color color;

  BarConfig(this.width, this.color);
}


class UrineColorBar extends StatelessWidget {
  final List<UrineEntry> entries;

  UrineColorBar({ Key key, this.entries }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<BarConfig> bars = getBarConfig(entries);

    if (entries.isEmpty) { return Container(); }

    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Row(
        children: [
          ...bars.map((bar) => SizedBox(
            width: bar.width,
            child: Container(width: 0, height: 15, color: bar.color)
          )),
        ],
      ),
    );
  }
}

List<BarConfig> getBarConfig(List<UrineEntry> entries) {
  List<int> volumes = entries
    .map((entry) => entry.volume)
    .toList();

  int overallMls = (volumes.isEmpty ? [0] : volumes)
    .reduce((value, mls) => value + mls);

  double mlWidthUnit = overallMls / maxWidth;

  Map<int, int> mlsPerColor = {
    5: 0,
    4: 0,
    3: 0,
    2: 0,
    1: 0,
    0: 0,
  };

  entries.forEach((entry) {
    mlsPerColor[entry.colorId] += entry.volume;
  });

  return mlsPerColor.keys
    .where((key) => mlsPerColor[key] > 0)
    .map((key) {
      double width = mlsPerColor[key] / mlWidthUnit;
      Color color = urineColors
        .singleWhere((color) => color.id == key)
        .color;

      return BarConfig(width, color);
    })
    .toList();
}
