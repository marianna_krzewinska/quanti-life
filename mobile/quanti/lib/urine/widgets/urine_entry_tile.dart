import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../utils.dart';
import '../urine_model.dart';
import '../urine_utils.dart';
import 'urine_edit_dialog.dart';


class UrineEntryTile extends StatelessWidget {
  final UrineEntry entry;

  UrineEntryTile({Key key, this.entry}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color _color = urineColors
      .singleWhere((element) => element.id == entry.colorId)
      .color;

    return GestureDetector(
      onTap: () => showDialog(
        context: context,
        builder: (BuildContext context) =>
          UrineEditDialog(urineEntry: entry, createNew: false),
      ),
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: 0,
                  child: Container(height: 70),
                ),

                Text(
                  getHour(entry.timestamp),
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w300,
                  ),
                ),

                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        right: 5,
                        bottom: 10,
                      ),
                      child:
                        IconButton(
                          icon: Icon(
                            UrineIcons.toilet,
                            color: _color,
                            size: 40,
                          ),
                          onPressed: null,
                        ),
                    ),

                    Padding(
                      padding: EdgeInsets.only(
                        right: 30,
                      ),
                      child: Text(
                        '${entry.volume} ml',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
