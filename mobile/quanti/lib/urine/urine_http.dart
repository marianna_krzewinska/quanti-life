import '../auth.dart' as auth;
import '../configs/index.dart';
import '../http.dart';
import 'urine_model.dart';


Future<List<UrineEntry>> getUrineEntries({ bool repeatOnAuthError = true }) async {
  List<dynamic> json = await http
    .sendGet(
      '${appConfig.serverUrl}/entries/urine',
      headers: await auth.getAuthHeaders(),
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return getUrineEntries(repeatOnAuthError: false);
      }

      throw error;
    });

  return json
    .map(UrineEntry.fromJSON)
    .toList();
}


Future<UrineSummary> getUrineEntriesSummary(
  DateTime start,
  DateTime end,
  { bool repeatOnAuthError = true }) async {

  dynamic json = await http
    .sendGet(
      '${appConfig.serverUrl}/entries/urine/summary' +
      '?start=${start.toIso8601String()}&end=${end.toIso8601String()}',
      headers: await auth.getAuthHeaders(),
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return getUrineEntriesSummary(start, end, repeatOnAuthError: false);
      }

      throw error;
    });

  return UrineSummary.fromJSON(json);
}



Future<UrineEntry> updateUrineEntry(
  UrineEntry urineEntry,
  { bool repeatOnAuthError = true }) async {

  dynamic data = await http
    .sendPut(
      '${appConfig.serverUrl}/entries/urine/${urineEntry.id}',
      body: urineEntry.toJSON(),
      headers: {
        'content-type': 'application/json',
        ...await auth.getAuthHeaders(),
      },
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return updateUrineEntry(urineEntry, repeatOnAuthError: false);
      }

      throw error;
    });

  return UrineEntry.fromJSON(data);
}


Future<String> deleteUrineEntry(
  String id,
  { bool repeatOnAuthError = true }) async {

  await http
    .sendDelete(
      '${appConfig.serverUrl}/entries/urine/$id',
      headers: await auth.getAuthHeaders(),
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return deleteUrineEntry(id, repeatOnAuthError: false);
      }

      throw error;
    });

  return id;
}


Future<UrineEntry> createUrineEntry(
  UrineEntry entry,
  { bool repeatOnAuthError = true }) async {

  dynamic data = await http
    .sendPost(
      '${appConfig.serverUrl}/entries/urine',
      body: entry.toJSON(),
      headers: {
        'content-type': 'application/json',
        ...await auth.getAuthHeaders(),
      },
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return createUrineEntry(entry, repeatOnAuthError: false);
      }

      throw error;
    });

  return UrineEntry.fromJSON(data);
}
