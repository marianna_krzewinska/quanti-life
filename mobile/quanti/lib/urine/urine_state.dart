import 'package:redux_epics/redux_epics.dart';

import '../auth.dart' as auth;
import '../main_state.dart';
import 'urine_http.dart' as http;
import 'urine_model.dart';


final UrineEntriesState initialUrineEntriesState = UrineEntriesState.initial();


final urineEntriesEpics = [
  getUrineEntriesEpic,
  createUrineEntryEpic,
  updateUrineEntryEpic,
  deleteUrineEntryEpic,
  getUrineEntriesSummaryEpic,
];


UrineEntriesState urineEntriesReducer(UrineEntriesState state, UrineAction action) {
  switch (action.type) {
    case UrineEntriesActions.GetListSuccess:
      return state.update(newList: action.list);

    case UrineEntriesActions.CreateSuccess:
      return state.update(newList: [...state.list, action.entry]);

    case UrineEntriesActions.UpdateSuccess:
      return state.update(newList: state.list
        .map((UrineEntry entry) {
          return entry.id == action.entry.id
            ? action.entry
            : entry;
        })
        .toList());

    case UrineEntriesActions.DeleteSuccess:
      return state.update(newList: state.list
        .where((UrineEntry entry) => entry.id != action.id)
        .toList());

    case UrineEntriesActions.GetSummary:
      return state.update(newSummary: UrineSummary.initial());

    case UrineEntriesActions.GetSummarySuccess:
      return state.update(newSummary: action.summary);

    default:
      break;
  }

  return state;
}


Stream<dynamic> getUrineEntriesEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == UrineEntriesActions.GetList)
    .asyncMap((action) async {
      List<UrineEntry> entries = await http.getUrineEntries();

      return GetUrineEntriesSuccessAction(entries);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => GetUrineEntriesFailAction());
}


Stream<dynamic> getUrineEntriesSummaryEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == UrineEntriesActions.GetSummary)
    .asyncMap((action) async {
      UrineSummary summary = await http.getUrineEntriesSummary(action.start, action.end);

      return GetUrineEntriesSummarySuccessAction(summary);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => GetUrineEntriesSummaryFailAction());
}


Stream<dynamic> createUrineEntryEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == UrineEntriesActions.Create)
    .asyncMap((action) async {
      UrineEntry entry = await http.createUrineEntry(action.entry);

      return CreateUrineEntrySuccessAction(entry);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => CreateUrineEntryFailAction());
}


Stream<dynamic> updateUrineEntryEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == UrineEntriesActions.Update)
    .asyncMap((action) async {
      UrineEntry json = await http.updateUrineEntry(action.entry);

      return UpdateUrineEntrySuccessAction(json);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => UpdateUrineEntryFailAction());
}


Stream<dynamic> deleteUrineEntryEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == UrineEntriesActions.Delete)
    .asyncMap((action) async {
      String id = await http.deleteUrineEntry(action.id);

      return DeleteUrineEntrySuccessAction(id);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => DeleteUrineEntryFailAction());
}


abstract class UrineAction {
  UrineEntriesActions type;

  List<UrineEntry> list;
  UrineEntry entry;
  String id;
  UrineSummary summary;
}


class GetUrineEntriesAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.GetList;

  GetUrineEntriesAction();
}


class GetUrineEntriesSuccessAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.GetListSuccess;
  List<UrineEntry> list;

  GetUrineEntriesSuccessAction(this.list);
}


class GetUrineEntriesFailAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.GetListFail;

  GetUrineEntriesFailAction();
}


class GetUrineEntriesSummaryAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.GetSummary;
  DateTime start;
  DateTime end;

  GetUrineEntriesSummaryAction(this.start, this.end);
}


class GetUrineEntriesSummarySuccessAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.GetSummarySuccess;
  UrineSummary summary;

  GetUrineEntriesSummarySuccessAction(this.summary);
}


class GetUrineEntriesSummaryFailAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.GetSummaryFail;

  GetUrineEntriesSummaryFailAction();
}


class CreateUrineEntryAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.Create;
  UrineEntry entry;

  CreateUrineEntryAction(this.entry);
}

class CreateUrineEntrySuccessAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.CreateSuccess;
  UrineEntry entry;

  CreateUrineEntrySuccessAction(this.entry);
}


class CreateUrineEntryFailAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.CreateFail;

  CreateUrineEntryFailAction();
}


class UpdateUrineEntryAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.Update;
  UrineEntry entry;

  UpdateUrineEntryAction(this.entry);
}


class UpdateUrineEntrySuccessAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.UpdateSuccess;
  UrineEntry entry;

  UpdateUrineEntrySuccessAction(this.entry);
}


class UpdateUrineEntryFailAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.UpdateFail;

  UpdateUrineEntryFailAction();
}


class DeleteUrineEntryAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.Delete;
  String id;

  DeleteUrineEntryAction(this.id);
}


class DeleteUrineEntrySuccessAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.DeleteSuccess;
  String id;

  DeleteUrineEntrySuccessAction(this.id);
}


class DeleteUrineEntryFailAction extends UrineAction {
  final UrineEntriesActions type = UrineEntriesActions.DeleteFail;

  DeleteUrineEntryFailAction();
}


enum UrineEntriesActions {
  GetList,
  GetListSuccess,
  GetListFail,
  Create,
  CreateSuccess,
  CreateFail,
  Update,
  UpdateSuccess,
  UpdateFail,
  Delete,
  DeleteSuccess,
  DeleteFail,
  GetSummary,
  GetSummarySuccess,
  GetSummaryFail,
}
