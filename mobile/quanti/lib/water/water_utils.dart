class IconConfig {
  double paddingLeft;
  double paddingRight;
  double iconSize;
  double fontSize;
  double textPadding;
  int volume;
  String text;

  IconConfig(
    this.paddingLeft,
    this.paddingRight,
    this.iconSize,
    this.fontSize,
    this.text,
    this.textPadding,
    this.volume,
  );

  IconConfig setPaddingRight(double newPaddingRight) => IconConfig(
    paddingLeft,
    newPaddingRight,
    iconSize,
    fontSize,
    text,
    textPadding,
    volume);
}

final Map<int, IconConfig> waterIconConfigs = {
  100: IconConfig(4.5, 0, 20, 20, '100 ml', 4, 100),
  200: IconConfig(4, 0, 30, 20, '200 ml', 4, 200),
  300: IconConfig(0, 0, 40, 20, '300 ml', 0, 300),
};
