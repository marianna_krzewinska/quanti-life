import 'dart:convert' as convert;

import '../auth.dart' as auth;
import '../configs/index.dart';
import '../http.dart';
import 'water_model.dart';


Future<List<WaterEntry>> getWaterEntries({ bool repeatOnAuthError = true }) async {
  List json = await http.sendGet(
      '${appConfig.serverUrl}/entries/water',
      headers: await auth.getAuthHeaders(),
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return getWaterEntries(repeatOnAuthError: false);
      }

      throw error;
    });

  return json.map(WaterEntry.fromJSON).toList();
}


Future<WaterSummary> getWaterEntriesSummary(
  DateTime start,
  DateTime end,
  { bool repeatOnAuthError = true }) async {

  dynamic json = await http.sendGet(
      '${appConfig.serverUrl}/entries/water/summary' +
      '?start=${start.toIso8601String()}&end=${end.toIso8601String()}',
      headers: await auth.getAuthHeaders(),
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return getWaterEntriesSummary(
          start,
          end,
          repeatOnAuthError: false);
      }

      throw error;
    });

  return WaterSummary.fromJSON(json);
}


Future<WaterEntry> updateWaterEntry(
  String id,
  int volume,
  DateTime timestamp,
  { bool repeatOnAuthError = true }) async {

  return await http.sendPut(
      '${appConfig.serverUrl}/entries/water/$id',
      body: convert.jsonEncode({
        'volume': volume,
        'timestamp': timestamp.toIso8601String(),
      }),
      headers: {
        'content-type': 'application/json',
        ...await auth.getAuthHeaders(),
      },
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .then(WaterEntry.fromJSON)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return updateWaterEntry(id, volume, timestamp, repeatOnAuthError: false);
      }

      throw error;
    });
}


Future<String> deleteWaterEntry(
  String id,
  { bool repeatOnAuthError = true }) async {

  await http.sendDelete(
      '${appConfig.serverUrl}/entries/water/$id',
      headers: await auth.getAuthHeaders(),
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return deleteWaterEntry(id, repeatOnAuthError: false);
      }

      throw error;
    });

  return id;
}


Future<WaterEntry> createWaterEntry(
  int volume,
  DateTime timestamp,
  { bool repeatOnAuthError = true }) async {

  dynamic data = await http.sendPost(
      '${appConfig.serverUrl}/entries/water',
      body: convert.jsonEncode({
        'volume': volume,
        'timestamp': timestamp.toIso8601String(),
      }),
      headers: {
        'content-type': 'application/json',
        ...await auth.getAuthHeaders(),
      },
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return createWaterEntry(volume, timestamp, repeatOnAuthError: false);
      }

      throw error;
    });

  return WaterEntry.fromJSON(data);
}
