import 'package:redux_epics/redux_epics.dart';

import '../auth.dart' as auth;
import '../main_state.dart';
import 'water_http.dart';
import 'water_model.dart';


final WaterEntriesState initialWaterEntriesState = WaterEntriesState.initial();


final waterEntriesEpics = [
  getWaterEntriesEpic,
  updateWaterEntryEpic,
  deleteWaterEntryEpic,
  createWaterEntryEpic,
  getWaterEntriesSummaryEpic,
];


WaterEntriesState waterEntriesReducer(WaterEntriesState entries, WaterAction action) {
  switch (action.type) {
    case WaterEntriesActions.GetListSuccess:
      return entries.update(newList: action.list);

    case WaterEntriesActions.CreateSuccess:
      return entries.update(
        newList: [...entries.list, action.entry],
      );

    case WaterEntriesActions.UpdateSuccess:
      return entries.update(newList: entries.list
        .map((WaterEntry entry) => entry.id == action.entry.id
          ? action.entry
          : entry)
        .toList());

    case WaterEntriesActions.DeleteSuccess:
      return entries.update(
        newList: entries.list
          .where((WaterEntry entry) => entry.id != action.id)
          .toList());

    case WaterEntriesActions.GetSummary:
      return entries.update(newSummary: WaterSummary.initial());

    case WaterEntriesActions.GetSummarySuccess:
      return entries.update(newSummary: action.summary);

    default:
      break;
  }

  return entries;
}


Stream<dynamic> getWaterEntriesEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == WaterEntriesActions.GetList)
    .asyncMap((action) async {
      List<WaterEntry> entries = await getWaterEntries();

      return GetWaterEntriesSuccessAction(entries);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => GetWaterEntriesFailAction());
}


Stream<dynamic> getWaterEntriesSummaryEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == WaterEntriesActions.GetSummary)
    .asyncMap((action) async {
      WaterSummary summary = await getWaterEntriesSummary(action.start, action.end);

      return GetWaterEntriesSummarySuccessAction(summary);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => GetWaterEntriesSummaryFailAction());
}


Stream<dynamic> updateWaterEntryEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == WaterEntriesActions.Update)
    .asyncMap((action) async {
      WaterEntry waterEntry = await updateWaterEntry(
        action.id,
        action.volume,
        action.timestamp);

      return UpdateWaterEntrySuccessAction(waterEntry);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => UpdateWaterEntryFailAction());
}


Stream<dynamic> createWaterEntryEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == WaterEntriesActions.Create)
    .asyncMap((action) async {
      WaterEntry waterEntry = await createWaterEntry(
        action.volume,
        DateTime.now());

      return CreateWaterEntrySuccessAction(waterEntry);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => CreateWaterEntryFailAction());
}


Stream<dynamic> deleteWaterEntryEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store) {

  return actions
    .where((action) => action.type == WaterEntriesActions.Delete)
    .asyncMap((action) async {
      String id = await deleteWaterEntry(action.id);

      return DeleteWaterEntrySuccessAction(id);
    })
    .handleError(auth.handleAuthError)
    .handleError((error) => DeleteWaterEntryFailAction());
}


abstract class WaterAction {
  WaterEntriesActions type;

  List<WaterEntry> list;
  WaterEntry entry;
  String id;
  WaterSummary summary;

  int volume;
  DateTime timestamp;
}


class GetWaterEntriesAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.GetList;

  GetWaterEntriesAction();
}


class GetWaterEntriesSuccessAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.GetListSuccess;
  List<WaterEntry> list;

  GetWaterEntriesSuccessAction(this.list);
}


class GetWaterEntriesFailAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.GetListFail;

  GetWaterEntriesFailAction();
}


class GetWaterEntriesSummaryAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.GetSummary;
  DateTime start;
  DateTime end;

  GetWaterEntriesSummaryAction(this.start, this.end);
}


class GetWaterEntriesSummarySuccessAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.GetSummarySuccess;
  WaterSummary summary;

  GetWaterEntriesSummarySuccessAction(this.summary);
}


class GetWaterEntriesSummaryFailAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.GetSummaryFail;

  GetWaterEntriesSummaryFailAction();
}


class CreateWaterEntryAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.Create;
  int volume;

  CreateWaterEntryAction(this.volume);
}


class CreateWaterEntrySuccessAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.CreateSuccess;
  WaterEntry entry;

  CreateWaterEntrySuccessAction(this.entry);
}


class CreateWaterEntryFailAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.CreateFail;

  CreateWaterEntryFailAction();
}


class UpdateWaterEntryAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.Update;
  String id;
  int volume;
  DateTime timestamp;

  UpdateWaterEntryAction(this.id, this.volume, this.timestamp);
}


class UpdateWaterEntrySuccessAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.UpdateSuccess;
  WaterEntry entry;

  UpdateWaterEntrySuccessAction(this.entry);
}


class UpdateWaterEntryFailAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.UpdateFail;

  UpdateWaterEntryFailAction();
}


class DeleteWaterEntryAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.Delete;
  String id;

  DeleteWaterEntryAction(this.id);
}


class DeleteWaterEntrySuccessAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.DeleteSuccess;
  String id;

  DeleteWaterEntrySuccessAction(this.id);
}


class DeleteWaterEntryFailAction extends WaterAction {
  final WaterEntriesActions type = WaterEntriesActions.DeleteFail;

  DeleteWaterEntryFailAction();
}


enum WaterEntriesActions {
  Create,
  CreateSuccess,
  CreateFail,
  Update,
  UpdateSuccess,
  UpdateFail,
  Delete,
  DeleteSuccess,
  DeleteFail,
  GetList,
  GetListSuccess,
  GetListFail,
  GetSummary,
  GetSummarySuccess,
  GetSummaryFail,
}
