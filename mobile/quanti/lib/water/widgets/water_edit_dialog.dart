import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

import '../../configs/base.dart';
import '../../main_state.dart';
import '../../utils.dart';
import '../water_model.dart';
import '../water_state.dart';
import '../water_utils.dart';


class WaterEditDialog extends StatefulWidget {
  final WaterEntry waterEntry;

  WaterEditDialog({Key key, this.waterEntry}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _WaterEditDialogState();
}


class _WaterEditDialogState extends State<WaterEditDialog> {

  int _selectedVolume;
  DateTime _selectedDate;
  TimeOfDay _selectedTime;

  @override
  void initState() {
    super.initState();

    _selectedVolume = widget.waterEntry.volume;
    _selectedDate = widget.waterEntry.timestamp;
    _selectedTime = TimeOfDay(
      hour: widget.waterEntry.timestamp.hour,
      minute: widget.waterEntry.timestamp.minute,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
      children: [
        Row(
          children: [
            GestureDetector(
              child: Text(
                formatDate(_selectedDate),
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  fontSize: 20,
                ),
              ),
              onTap: () => _handleChangeDate(context),
            ),

            GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(left: 20),
                child: Text(
                  formatTime(_selectedTime),
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 20,
                  ),
                ),
              ),
              onTap: () => _handleChangeTime(context),
            ),
          ]),

        Padding(
          padding: EdgeInsets.only(top: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 5),
                child: IconButton(
                  icon: Icon(Icons.local_drink, color: _getIconColor(100)),
                  iconSize: waterIconConfigs[100].iconSize,
                  onPressed: _getVolumeChangeCallback(100),
                ),
              ),

              IconButton(
                icon: Icon(Icons.local_drink, color: _getIconColor(200)),
                iconSize: waterIconConfigs[200].iconSize,
                onPressed: _getVolumeChangeCallback(200),
              ),

              Padding(
                padding: EdgeInsets.only(right: 5),
                child: IconButton(
                  icon: Icon(Icons.local_drink, color: _getIconColor(300)),
                  iconSize: waterIconConfigs[300].iconSize,
                  onPressed: _getVolumeChangeCallback(300),
                ),
              ),
            ],
          ),
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 5),
              child: Text(
                waterIconConfigs[100].text,
                style: TextStyle(
                  fontSize: 15,
                  color: _getTextColor(100),
                ),
              ),
            ),

            Text(
              waterIconConfigs[200].text,
              style: TextStyle(
                fontSize: 15,
                color: _getTextColor(200),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(right: 5),
              child: Text(
                waterIconConfigs[300].text,
                style: TextStyle(
                  fontSize: 15,
                  color: _getTextColor(300),
                ),
              ),
            ),
          ],
        ),

        Padding(
          padding: EdgeInsets.only(top: 40),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              StoreConnector<AppState, Function>(
                builder: (BuildContext context, Function handleDelete) => IconButton(
                  iconSize: 35,
                  icon: Icon(Icons.delete_outline),
                  onPressed: handleDelete),
                converter: (store) => () => _delete(store)),

              StoreConnector<AppState, Function>(
                builder: (BuildContext context, Function handleSave) => IconButton(
                  iconSize: 35,
                  icon: Icon(Icons.done),
                  onPressed: handleSave),
                converter: (store) => () => _save(store)),
            ],
          ),
        ),
      ],
    );
  }

  void _delete(store) {
    store.dispatch(DeleteWaterEntryAction(widget.waterEntry.id));
    store.dispatch(NavigateToAction.pop());
  }

  void _save(store) {
    store.dispatch(UpdateWaterEntryAction(
      widget.waterEntry.id,
      _selectedVolume,
      DateTime(
        _selectedDate.year,
        _selectedDate.month,
        _selectedDate.day,
        _selectedTime.hour,
        _selectedTime.minute,
      )
    ));

    store.dispatch(NavigateToAction.pop());
  }

  void _handleChangeDate(BuildContext context) async {
    DateTime newDate = await showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: _selectedDate.subtract(Duration(days: 365)),
      lastDate: DateTime.now()
    );

    setState(() {
      _selectedDate = newDate ?? _selectedDate;
    });
  }

  void _handleChangeTime(BuildContext context) async {
    TimeOfDay newTime = await showTimePicker(
      context: context,
      initialTime: _selectedTime,
    );

    setState(() {
      _selectedTime = newTime ?? _selectedTime;
    });
  }

  Color _getTextColor(int buttonVolume) => _selectedVolume == buttonVolume
    ? Colors.white
    : Colors.white.withOpacity(0.4);

  Color _getIconColor(int buttonVolume) => _selectedVolume == buttonVolume
    ? ColorMap.water
    : ColorMap.water.withOpacity(0.4);

  Function _getVolumeChangeCallback(int volume) => () => setState(() {
    _selectedVolume = volume;
  });
}
