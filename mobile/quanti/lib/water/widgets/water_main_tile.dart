import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../configs/base.dart';
import '../../main_state.dart';
import '../../utils.dart';
import '../water_model.dart';
import '../water_state.dart';
import '../water_utils.dart';
import 'water_add_button.dart';


class WaterMainTile extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);
    store.dispatch(GetWaterEntriesAction());

    return Card(
      shape: ContinuousRectangleBorder(borderRadius: BorderRadius.zero),
      margin: EdgeInsets.only(top: 20, left: 15, right: 15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 10,
                child: Container(width: 0, height: 150),
              ),

              StoreConnector<AppState, int>(
                converter: (store) => _getTodaysMl(store.state.waterEntries.list),
                builder: (BuildContext context, int ml) => Text(
                  '$ml ml',
                  style: TextStyle(
                    color: ColorMap.water,
                    fontSize: 40,
                    fontWeight: FontWeight.w300,
                  ),
                )),

              Padding(
                padding: const EdgeInsets.only(right: 20, top: 5, bottom: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AddWaterButton(config: waterIconConfigs[100]),
                    AddWaterButton(config: waterIconConfigs[200]),
                    AddWaterButton(config: waterIconConfigs[300]),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  int _getTodaysMl(List<WaterEntry> waterEntries) {
    DateTime today = justDate(DateTime.now());

    List<int> volumes = waterEntries
      .where((entry) => entry.timestamp.isAfter(today))
      .map((entry) => entry.volume)
      .toList();

    return (volumes.isEmpty ? [0] : volumes)
      .reduce((value, volume) => value + volume);
  }
}
