import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../water_utils.dart';
import '../../configs/base.dart';


class WaterSizedButton extends StatelessWidget {
  final IconConfig config;

  WaterSizedButton({Key key, this.config}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: config.paddingLeft,
        right: config.paddingRight,
      ),
      child: Row(
        children: [
          IconButton(
            icon: Icon(Icons.local_drink, color: ColorMap.water),
            iconSize: config.iconSize,
            onPressed: null,
          ),

          Padding(
            padding: EdgeInsets.only(left: config.textPadding),
            child: Text(
              config.text,
              style: TextStyle(
                fontSize: config.fontSize,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
