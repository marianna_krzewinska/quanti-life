import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../main_state.dart';
import '../water_state.dart';
import '../water_utils.dart';
import 'water_sized_button.dart';


class AddWaterButton extends StatelessWidget {
  final IconConfig config;

  AddWaterButton({Key key, this.config}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Function>(
      converter: (store) =>
        (int value) => store.dispatch(CreateWaterEntryAction(value)),
      builder: (BuildContext context, Function onTap) => GestureDetector(
        onTap: () => onTap(config.volume),
        child: WaterSizedButton(config: config)
      )
    );
  }
}
