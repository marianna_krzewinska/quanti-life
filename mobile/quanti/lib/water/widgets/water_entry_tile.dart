import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../utils.dart';
import '../water_model.dart';
import '../water_utils.dart';
import 'water_edit_dialog.dart';
import 'water_sized_button.dart';


class WaterEntryTile extends StatelessWidget {
  final WaterEntry waterEntry;

  WaterEntryTile({ Key key, this.waterEntry }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    IconConfig config = waterIconConfigs[waterEntry.volume]
      .setPaddingRight(30);

    return GestureDetector(
      onTap: () => showDialog(
        context: context,
        builder: (BuildContext context) => WaterEditDialog(waterEntry: waterEntry),
      ),

      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: 0,
                  child: Container(height: 70),
                ),

                Text(
                  getHour(waterEntry.timestamp),
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 30
                  ),
                ),

                WaterSizedButton(config: config),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
