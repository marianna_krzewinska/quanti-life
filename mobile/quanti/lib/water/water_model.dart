import 'dart:convert';

import '../entries/entries_model.dart';


class WaterEntriesState {
  List<WaterEntry> list;
  WaterSummary summary;

  WaterEntriesState(this.list, this.summary);

  static WaterEntriesState initial() { return WaterEntriesState([], WaterSummary.initial()); }

  WaterEntriesState update({
    List<WaterEntry> newList,
    WaterSummary newSummary,
  }) {
    return WaterEntriesState(
      newList ?? list,
      newSummary ?? summary,
    );
  }
}


class WaterAggregate {
  int volume;
  DateTime timestamp;

  WaterAggregate(this.volume, this.timestamp);
}


List<WaterAggregate> parseAggregates(List data) {
  return data
    .map((dynamic entry) => WaterAggregate(
      entry['volume'],
      DateTime.parse(entry['timestamp'])))
    .toList();
}


class WaterSummary {
  DateTime start;
  DateTime end;
  List<WaterAggregate> aggregates = [];

  WaterSummary({ this.start, this.end, this.aggregates });

  static WaterSummary initial() => WaterSummary(
    start: null,
    end: null,
    aggregates: []);

  static WaterSummary fromJSON(dynamic data) => data is WaterSummary
    ?  data
    : WaterSummary(
      start: DateTime.parse(data['start']),
      end: DateTime.parse(data['end']),
      aggregates: parseAggregates(data['aggregates']),
    );
}


class WaterEntry extends Entry {
  String id;
  int volume;
  DateTime timestamp;
  DateTime updatedAt;
  DateTime createdAt;
  EntryType type = EntryType.INGESTION_FOOD_WATER;

  WaterEntry(
    this.id,
    this.volume,
    this.updatedAt,
    this.createdAt,
    this.timestamp): super(timestamp);

  static WaterEntry fromJSON(entry) => entry is WaterEntry
    ? entry
    : WaterEntry(
      entry['id'],
      entry['volume'],
      DateTime.parse(entry['updated_at']),
      DateTime.parse(entry['created_at']),
      DateTime.parse(entry['timestamp']),
    );

  String toJSON() => jsonEncode({
    'volume': volume,
    'timestamp': timestamp.toIso8601String(),
  });
}
