class User {
  String displayName;
  String firstName;
  String lastName;
  String email;
  String id;
  String photoUrl;
  DateTime createdAt;
  DateTime updatedAt;

  User(
    this.id,
    this.displayName,
    this.firstName,
    this.lastName,
    this.email,
    this.photoUrl,
    this.createdAt,
    this.updatedAt,
  );

  static User fromJSON(dynamic data) {
    return data is User ? data : User(
      data['id'],
      data['display_name'],
      data['first_name'],
      data['last_name'],
      data['email'],
      data['picture_url'],
      DateTime.parse(data['created_at']),
      DateTime.parse(data['updated_at']),
    );
  }
}

class UserData {
  User user;
  bool isAuthorizing;

  UserData(this.user, this.isAuthorizing);

  static UserData initialize() => UserData(
    User(null, null, null, null, null, null, DateTime.now(), DateTime.now()),
    null,
  );

  UserData update({ User newUser, bool newIsAuthorizing }) {
    return UserData(
      newUser ?? user,
      newIsAuthorizing ?? isAuthorizing,
    );
  }
}
