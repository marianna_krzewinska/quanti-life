import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

import '../main_state.dart';
import 'user_model.dart';
import 'user_state.dart';


class UserSettingsView extends StatelessWidget {
  static String route = '/settings';

  @override
  Widget build(BuildContext context) {

    final store = StoreProvider.of<AppState>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
        centerTitle: true,
      ),

      body: StoreConnector<AppState, UserData>(
        builder: (BuildContext context, UserData userData) => ListView(
          children: [
            Card(
              shape: ContinuousRectangleBorder(borderRadius: BorderRadius.zero),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: Text(
                      '${ userData.user.email }',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w300
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Card(
              child: IconButton(
                icon: Icon(Icons.logout),
                onPressed: () => store.dispatch(LogoutAction()),
              ),
            ),
          ],
        ),
        converter: (store) => store.state.userData,
      ),
    );
  }
}
