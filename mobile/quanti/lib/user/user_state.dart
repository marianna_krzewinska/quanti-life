import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:redux_epics/redux_epics.dart';

import '../auth.dart' as auth;
import '../google_sign_in.dart';
import '../home/home_view.dart';
import '../login_view.dart';
import '../main_state.dart';
import 'user_http.dart' as http;
import 'user_model.dart';


final UserData initialUserDataState = UserData.initialize();


final userDataEpics = [
  loginEpic,
  getUserDataEpic,
  logoutEpic,
];


UserData userDataReducer(UserData userData, action) {
  switch (action.type) {
    case UserActions.GetSuccess:
      return userData.update(newUser: action.user);
    default:
      break;
  }

  return userData;
}


Stream<dynamic> getUserDataEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions
    .where((action) => action.type == UserActions.Get)
    .asyncMap((action) async {
      User account;
      try {
        account = await http.getUserData();
      } catch(e) { return [auth.handleAuthError(e)]; }

      if (account == null) {
        return [LogoutAction()];
      }

      return [
        action.action ?? EmptyAction(),
        GetUserSuccessAction(account),
      ];
    })
    .asyncExpand((actions) => Stream.fromIterable(actions));
}


Stream<dynamic> loginEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions
    .where((action) => action.type == UserActions.Login)
    .asyncMap((action) async {
      await auth.resetAuthHeaders();
      GoogleSignInAccount account;

      try {
        account = await googleSignIn.signIn();
      } catch(e) {}

      if (account == null) {
        return [LogoutAction()];
      }

      return [
        GetUserAction(action: NavigateToAction
          .pushNamedAndRemoveUntil(HomeView.route, (r) => false)),
      ];
    })
    .asyncExpand((actions) => Stream.fromIterable(actions));
}


Stream<dynamic> logoutEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions
    .where((action) => action.type == UserActions.Logout)
    .asyncMap((action) async {
      try {
        await auth.resetAuthHeaders();
        await googleSignIn.disconnect();
      } catch (e) {}

      return [
        ClearState(),
        NavigateToAction.pushNamedAndRemoveUntil(LoginView.route, (r) => false),
      ];
    })
    .expand((actions) => actions);
}


abstract class UserAction {
  UserActions type;
}


class LogoutAction extends UserAction {
  final UserActions type = UserActions.Logout;

  LogoutAction();
}


class GetUserAction extends UserAction {
  UserActions type = UserActions.Get;
  dynamic action;

  GetUserAction({ this.action });
}


class GetUserSuccessAction extends UserAction {
  UserActions type = UserActions.GetSuccess;
  User user;

  GetUserSuccessAction(this.user);
}


class LoginAction extends UserAction {
  UserActions type = UserActions.Login;

  LoginAction();
}


enum UserActions {
  Get,
  GetSuccess,
  Login,
  Logout,
}
