import '../auth.dart' as auth;
import '../configs/index.dart';
import '../http.dart';
import 'user_model.dart';


Future<User> getUserData({ bool repeatOnAuthError = true }) async {
  dynamic json = await http
    .sendGet(
      '${appConfig.serverUrl}/users/me',
      headers: await auth.getAuthHeaders(),
    )
    .then(auth.handleHttpErrorResponse)
    .then(auth.handleHttpSuccessResponse)
    .catchError((error) async {
      if (error is auth.UnauthorizedException && repeatOnAuthError == true) {
        await auth.resetAuthHeaders();
        return getUserData(repeatOnAuthError: false);
      }

      throw error;
    });

  return User.fromJSON(json);
}
