import 'package:flutter/material.dart';

class ColorMap {
  static final Color bg = Color.fromARGB(255, 18, 18, 18);
  static final Color water = Color.fromARGB(255, 144, 202, 249);
  static final Color urine = Color.fromARGB(255, 255, 245, 157);
  static final String waterHex = '#90CAF9';
}


class AppConfig {
  final String serverUrl;

  AppConfig({ this.serverUrl });
}
