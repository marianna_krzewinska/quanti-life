import 'configs/dev.dart' as Env;
import 'main.dart' as App;


void main() async {
  final config = Env.devConfig;

  App.main(config);
}
