import 'configs/local.dart' as Env;
import 'main.dart' as App;


void main() async {
  final config = Env.localConfig;

  App.main(config);
}
