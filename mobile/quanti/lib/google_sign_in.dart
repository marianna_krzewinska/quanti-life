import 'package:google_sign_in/google_sign_in.dart';
import 'package:mockito/mockito.dart';

import 'utils.dart';


final String mockAccessToken = 'accessToken';
final String mockIdToken = 'idToken';


class GoogleSignInMockup extends Mock implements GoogleSignIn {}


class MockGoogleSignInAccount extends Mock implements GoogleSignInAccount {
  @override
  Future<GoogleSignInAuthentication> get authentication =>
      Future.value(MockGoogleSignInAuthentication());

  @override
  bool operator ==(other) {
    return (other is Mock)
      ? hashCode == other.hashCode
      : identical(this, other);
  }
}


class MockGoogleSignInAuthentication extends Mock implements GoogleSignInAuthentication {
  @override
  String get idToken => mockIdToken;

  @override
  String get accessToken => mockAccessToken;
}


final GoogleSignIn googleSignIn = isTest ? GoogleSignInMockup() : GoogleSignIn(
  scopes: <String>[
    'email',
    'profile',
    'openid',
  ],
);
