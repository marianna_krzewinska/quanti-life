import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

import '../main_state.dart';
import '../urine/urine_model.dart';
import '../urine/widgets/urine_entry_tile.dart';
import '../utils.dart';
import '../water/water_model.dart';
import '../water/widgets/water_entry_tile.dart';
import 'date_row.dart';
import 'entries_model.dart';


class EntriesMap {
  List<WaterEntry> waterEntries;
  List<UrineEntry> urineEntries;

  EntriesMap(this.waterEntries, this.urineEntries);
}


class EntryMap {
  Widget widget;
  Entry entry;

  EntryMap(this.widget, this.entry);
}


class EntriesView extends StatelessWidget {
  static String route = '/entries';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Entries'),
        centerTitle: true,
      ),
      body: StoreConnector<AppState, EntriesMap>(
        converter: (store) {
          return EntriesMap(
            store.state.waterEntries.list,
            store.state.urineEntries.list,
          );
        },
        builder: (BuildContext context, EntriesMap entries) {
          return Padding(
            padding: EdgeInsets.only(top: 10),
            child: ListView(
              children: [
                ...generateListOfEntries(entries),
              ],
            ));
        }),
    );
  }
}


List<Widget> generateListOfEntries(EntriesMap entries) {
  List<EntryMap> sortedList = [
    ...entries.waterEntries
      .map((entry) => EntryMap(WaterEntryTile(waterEntry: entry), entry)),
    ...entries.urineEntries
      .map((entry) => EntryMap(UrineEntryTile(entry: entry), entry)),
  ]
  ..sort((a, b) =>
    b.entry.timestamp.compareTo(a.entry.timestamp));

  List<Widget> result = [];
  DateTime lastDate = justDate(DateTime.now());

  for (EntryMap entryMap in sortedList) {
    if (entryMap.entry.timestamp.isAfter(lastDate)) {
      result.add(entryMap.widget);
    } else {
      lastDate = justDate(entryMap.entry.timestamp);
      result.add(DateRow(date: lastDate));
      result.add(entryMap.widget);
    }
  }

  return result.toList();
}
