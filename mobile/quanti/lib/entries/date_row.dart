import 'package:flutter/material.dart';

import '../utils.dart';


class DateRow extends StatelessWidget {
  final DateTime date;

  DateRow({Key key, this.date}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateTime todayMidnight = justDate(DateTime.now());

    if (date.isAfter(todayMidnight) || date.isAtSameMomentAs(todayMidnight)) {
      return Container();
    }

    DateTime yesterdayMidnight = todayMidnight.subtract(Duration(days: 1));

    bool isYesterday = date.isAtSameMomentAs(yesterdayMidnight);

    String dateRowText = isYesterday ? 'Yesterday' : formatDate(date);

    return Padding(
      padding: EdgeInsets.only(bottom: 15, top: 15, left: 20, right: 20),
      child: Row(
        children: [
          Text(
            dateRowText,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w300
            ),
          ),
        ],
      )
    );
  }
}
