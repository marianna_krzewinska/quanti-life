class Entry {
  DateTime timestamp;
  EntryType type;

  Entry(this.timestamp);
}

enum EntryType {
  INGESTION_FOOD_WATER,
  EXCRETION_URINE,
}
