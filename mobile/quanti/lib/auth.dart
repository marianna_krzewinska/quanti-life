import 'dart:convert' as convert;
import 'dart:io';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart';

import 'google_sign_in.dart';
import 'home/home_view.dart';
import 'main_state.dart';
import 'shared_preferences.dart';
import 'user/user_state.dart';


const _authTokenKey = 'auth-token';


class AuthView extends StatelessWidget {
  static String route = '/auth';

  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);

    store.dispatch(
      GetUserAction(
        action: NavigateToAction
          .pushNamedAndRemoveUntil(HomeView.route, (r) => false)));

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SpinKitFadingCube(color: Colors.blue[600])
            ],
          ),
        ],
      ),
    );
  }
}

Map<String, String> createAuthHeader(String token) => ({
  'Authorization': token,
});


Future<Map<String, String>> getAuthHeaders() async {
  String token = sharedPreferences.getString(_authTokenKey);

  if (token != null) {
    return createAuthHeader(token);
  }

  GoogleSignInAccount account;

  try {
    account = await googleSignIn.signInSilently();
  } catch (exception) {}

  if (account == null) {
    return createAuthHeader('');
  }

  String newToken = (await account.authentication).accessToken;

  await sharedPreferences.setString(_authTokenKey, newToken);

  return createAuthHeader(newToken);
}


Future<bool> resetAuthHeaders() => sharedPreferences.setString(_authTokenKey, null);


LogoutAction handleAuthError(dynamic exception) {
  if (exception is UnauthorizedException) {
    return LogoutAction();
  }

  throw exception;
}


Response handleHttpErrorResponse(Response response) {
  switch(response.statusCode) {
    case HttpStatus.ok:
    case HttpStatus.created:
      return response;
    case HttpStatus.forbidden:
    case HttpStatus.unauthorized:
      throw UnauthorizedException(message: response.reasonPhrase);
    default:
      throw HttpCustomException(message: response.reasonPhrase ?? response.body);
  }
}

dynamic handleHttpSuccessResponse(dynamic response) {
  dynamic data;

  try {
    data = convert.jsonDecode(response.body);
  } catch (e) {
    data = response.body;
  }

  return data;
}

class HttpCustomException implements Exception {
  final message;
  final prefix;

  HttpCustomException({ this.message, this.prefix });

  String toString() {
    return '$prefix$message';
  }
}


class UnauthorizedException extends HttpCustomException {
  UnauthorizedException({ String message }):
    super(message: message, prefix: 'Unauthorized: ');
}
