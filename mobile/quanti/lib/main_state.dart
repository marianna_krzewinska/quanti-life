import 'package:redux_epics/redux_epics.dart';

import 'summary/summary_model.dart';
import 'summary/summary_state.dart';
import 'urine/urine_model.dart';
import 'urine/urine_state.dart';
import 'user/user_model.dart';
import 'user/user_state.dart';
import 'water/water_model.dart';
import 'water/water_state.dart';


class AppState {
  final WaterEntriesState waterEntries;
  final UrineEntriesState urineEntries;
  final UserData userData;
  final SummarySettings summarySettings;

  AppState(
    this.waterEntries,
    this.urineEntries,
    this.userData,
    this.summarySettings);

  factory AppState.initial() => AppState(
    initialWaterEntriesState,
    initialUrineEntriesState,
    initialUserDataState,
    SummarySettings.initial(),
  );
}


final epics = combineEpics<AppState>([
  ...waterEntriesEpics,
  ...urineEntriesEpics,
  ...userDataEpics,
  ...summaryEpics,
]);


AppState reducer(AppState state, action) {
  if (action is WaterAction) {
    return AppState(
      waterEntriesReducer(state.waterEntries, action),
      state.urineEntries,
      state.userData,
      state.summarySettings,
    );
  }

  if (action is UrineAction) {
    return AppState(
      state.waterEntries,
      urineEntriesReducer(state.urineEntries, action),
      state.userData,
      state.summarySettings,
    );
  }

  if (action is UserAction) {
    return AppState(
      state.waterEntries,
      state.urineEntries,
      userDataReducer(state.userData, action),
      state.summarySettings
    );
  }

  if (action is SummaryAction) {
    return AppState(
      state.waterEntries,
      state.urineEntries,
      state.userData,
      summaryReducer(state.summarySettings, action),
    );
  }

  switch (action.type) {
    case MainActions.ClearState:
      return AppState.initial();
  }

  return state;
}


class EmptyAction {
  final MainActions type = MainActions.Empty;

  EmptyAction();
}


class ClearState {
  final MainActions type = MainActions.ClearState;

  ClearState();
}


enum MainActions {
  Empty,
  ClearState,
}
