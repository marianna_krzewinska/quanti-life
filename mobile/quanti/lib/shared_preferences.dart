import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'utils.dart';


class MockSharedPreferences extends Mock implements SharedPreferences {}


SharedPreferences sharedPreferences;


Future<void> initializeSharedPreferences() async {
  sharedPreferences = isTest ? MockSharedPreferences() : await SharedPreferences.getInstance();
}
