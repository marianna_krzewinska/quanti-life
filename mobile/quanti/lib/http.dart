import 'dart:convert';

import 'package:http/http.dart' as httpClient;
import 'package:mockito/mockito.dart';

import 'utils.dart';


class Http {
  sendGet(dynamic url, { Map<String, String> headers }) =>
    httpClient.get(url, headers: headers);
  sendPut(
    dynamic url,
    { Map<String, String> headers, Encoding encoding, dynamic body }) =>
    httpClient.put(url, headers: headers, encoding: encoding, body: body);
  sendPost(
    dynamic url,
    { Map<String, String> headers, Encoding encoding, dynamic body }) =>
    httpClient.post(url, headers: headers, encoding: encoding, body: body);
  sendDelete(dynamic url, { Map<String, String> headers }) =>
    httpClient.delete(url, headers: headers);
  sendPatch(
    dynamic url,
    { Map<String, String> headers, Encoding encoding, dynamic body }) =>
    httpClient.patch(url, headers: headers, encoding: encoding, body: body);
}


class MockHttp extends Mock implements Http {}


final http = isTest ? MockHttp() : Http();
