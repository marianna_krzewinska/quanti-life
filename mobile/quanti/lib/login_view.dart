import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'main_state.dart';
import 'user/user_state.dart';


class LoginView extends StatelessWidget {
  static String route = '/login';

  @override
  Widget build(BuildContext context) {

    final store = StoreProvider.of<AppState>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        actions: [],
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              TextButton(
                onPressed: () { store.dispatch(LoginAction()); },
                child: Text('LOGIN')),
            ],
          )
        ],
      ),
    );
  }
}
