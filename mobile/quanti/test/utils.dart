import 'dart:io';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main.dart';
import 'package:quanti/configs/index.dart';
import 'package:quanti/configs/test.dart';
import 'package:quanti/main_state.dart';
import 'package:quanti/shared_preferences.dart';


Future setupEnvironment() async {
  initializeConfig(testConfig);
  await initializeSharedPreferences();

  when(sharedPreferences.getString('auth-token')).thenReturn('aaa.bbb.ccc');
  when(sharedPreferences.setString('auth-token', null)).thenAnswer((_) async => true);
}


final unauthorizedResponse = Response(
  'unauthorized',
  HttpStatus.unauthorized,
  reasonPhrase: 'unauthorized',
);


Future initializeApp(WidgetTester tester, { AppState initialState }) async {
  final epicMiddleware = EpicMiddleware<AppState>(epics);

  final store = Store<AppState>(
    reducer,
    initialState: initialState ?? AppState.initial(),
    middleware: [
      epicMiddleware,
      NavigationMiddleware<AppState>(),
    ]
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: App(store: store)),
    Duration(seconds: 1000),
  );
}
