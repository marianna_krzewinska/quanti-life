import 'package:flutter_test/flutter_test.dart';

import 'package:quanti/user/user_model.dart';


void main() {
  group('"User"', () {
    test('should be initialized with provided parameters', () {
      final date = DateTime(2010, 2, 26, 2, 14, 3);

      final id = 'id';
      final displayName = 'test user';
      final firstName = 'test';
      final lastName = 'user';
      final email = 'test@user.email';
      final photoUrl = '';
      final createdAt = date;
      final updatedAt = date;

      final user = User(
        id,
        displayName,
        firstName,
        lastName,
        email,
        photoUrl,
        createdAt,
        updatedAt,
      );

      expect(user.id, id, reason: 'id is incorrect');
      expect(user.displayName, displayName, reason: 'displayName is incorrect');
      expect(user.firstName, firstName, reason: 'firstName is incorrect');
      expect(user.lastName, lastName, reason: 'lastName is incorrect');
      expect(user.email, email, reason: 'email is incorrect');
      expect(user.photoUrl, photoUrl, reason: 'photoUrl is incorrect');
      expect(user.createdAt, createdAt, reason: 'createdAt is incorrect');
      expect(user.updatedAt, updatedAt, reason: 'updatedAt is incorrect');
    });

    group('"fromJSON"', () {
      test('should parse dynamic map to User object', () {
        final user = User.fromJSON(_mockUserData);

        expect(user.id, _mockUserData['id'], reason: 'id is incorrect');
        expect(user.displayName, _mockUserData['display_name'],
          reason: 'displayName is incorrect');
        expect(user.firstName, _mockUserData['first_name'],
          reason: 'firstName is incorrect');
        expect(user.lastName, _mockUserData['last_name'],
          reason: 'lastName is incorrect');
        expect(user.email, _mockUserData['email'], reason: 'email is incorrect');
        expect(user.photoUrl, _mockUserData['picture_url'],
          reason: 'photoUrl is incorrect');
        expect(user.createdAt.toIso8601String(), _mockUserData['created_at'],
          reason: 'createdAt is incorrect');
        expect(user.updatedAt.toIso8601String(), _mockUserData['updated_at'],
          reason: 'updatedAt is incorrect');
      });
    });
  });

  group('"UserData"', () {
    test('should be initialized with provided parameters', () {
      final user = User.fromJSON(_mockUserData);

      final userData = UserData(user, true);

      expect(userData.user, user, reason: 'incorrect user property');
      expect(userData.isAuthorizing, true, reason: 'incorrect isAuthorizing property');
    });

    group('"initialize"', () {
      test('should return user data object filled with nulls', () {
        final userData = UserData.initialize();
        final user = userData.user;

        expect(user.id, null, reason: 'user id is not null');
        expect(user.displayName, null, reason: 'user displayName is not null');
        expect(user.firstName, null, reason: 'user firstName is not null');
        expect(user.lastName, null, reason: 'user lastName is not null');
        expect(user.email, null, reason: 'user email is not null');
        expect(user.photoUrl, null, reason: 'user photoUrl is not null');
        expect(user.createdAt is DateTime, true, reason: 'user createdAt is not date');
        expect(user.updatedAt is DateTime, true, reason: 'user updatedAt is not date');

        expect(userData.isAuthorizing, null, reason: 'isAuthorizing is not null');
      });
    });

    group('"update"', () {
      test('should return new user data instance with updated properties', () {
        final originalUserData = UserData.initialize();
        final user = User.fromJSON(_mockUserData);
        final updated = originalUserData.update(newUser: user, newIsAuthorizing: true);

        expect(updated == originalUserData, false,
          reason: 'user data instance was not changed');
        expect(updated.user, user, reason: 'incorrect user property');
        expect(updated.isAuthorizing, true, reason: 'incorrect isAuthorizing property');
      });
    });
  });
}


// DATA & helper functions


final _mockDate = DateTime(2010, 2, 26, 2, 14, 3);

final _mockUserData = {
  'id': 'id',
  'display_name': 'test user',
  'first_name': 'test',
  'last_name': 'user',
  'email': 'test@email.email',
  'picture_url': '',
  'created_at': _mockDate.toIso8601String(),
  'updated_at': _mockDate.toIso8601String(),
};
