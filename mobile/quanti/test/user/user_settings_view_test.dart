import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/summary/summary_model.dart';
import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/user/user_model.dart';
import 'package:quanti/user/user_settings_view.dart';
import 'package:quanti/water/water_model.dart';

import '../utils.dart';


void main() {
  group('"UserSettings"', () {
    testWidgets('"Should display an email and a logout button"',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeUserSettings(tester);

        expect(find.text('${_user.email}'), findsOneWidget,
          reason: 'user email not found');
        expect(find.byIcon(Icons.logout), findsOneWidget,
          reason: 'logout button not found');
      });
    });
  });
}


// DATA & helper functions


final _user = User(
  '123',
  'test user',
  'test',
  'user',
  'test@user.email',
  '',
  DateTime.now(),
  DateTime.now(),
);


Future initializeUserSettings(WidgetTester tester) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState(
      WaterEntriesState.initial(),
      UrineEntriesState.initial(),
      UserData(_user, false),
      SummarySettings.initial(),
    )
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: UserSettingsView.route,
      routes: {
        UserSettingsView.route: (_) => UserSettingsView(),
      })),
    Duration(seconds: 1000),
  );
}
