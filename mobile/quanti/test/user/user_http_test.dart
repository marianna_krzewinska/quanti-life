import 'dart:convert';
import 'dart:io';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';

import 'package:quanti/auth.dart';
import 'package:quanti/configs/index.dart';
import 'package:quanti/http.dart';
import 'package:quanti/user/user_http.dart';
import 'package:quanti/user/user_model.dart';

import '../utils.dart';


void main() {
  group('"getUserData"', () {
    testWidgets('should return user data when successful',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendGet(
          '${appConfig.serverUrl}/users/me', headers: anyNamed('headers')))
          .thenAnswer((_) async => Response(_userJSON, HttpStatus.ok));

        final result = await getUserData();

        verify(http.sendGet(
          '${appConfig.serverUrl}/users/me', headers: anyNamed('headers')))
          .called(1);

        expect(result.id, _mockUser.id, reason: 'incorrect id');
        expect(result.displayName, _mockUser.displayName, reason: 'incorrect displayName');
        expect(result.firstName, _mockUser.firstName, reason: 'incorrect firstName');
        expect(result.lastName, _mockUser.lastName, reason: 'incorrect lastName');
        expect(result.email, _mockUser.email, reason: 'incorrect email');
        expect(result.photoUrl, _mockUser.photoUrl, reason: 'incorrect photoUrl');
        expect(result.createdAt, _mockUser.createdAt, reason: 'incorrect createdAt');
        expect(result.updatedAt, _mockUser.updatedAt, reason: 'incorrect updatedAt');
      });
    });

    testWidgets('should repeat on UnauthorizedException',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        int execs = 0;

        when(http.sendGet(
          '${appConfig.serverUrl}/users/me', headers: anyNamed('headers')))
          .thenAnswer((_) async {
            execs += 1;

            if (execs == 1) { return unauthorizedResponse; }

            return Response(_userJSON, HttpStatus.ok);
          });

        final result = await getUserData();

        verify(http.sendGet(
          '${appConfig.serverUrl}/users/me', headers: anyNamed('headers')))
          .called(2);

        expect(result.id, _mockUser.id, reason: 'incorrect id');
      });
    });

    testWidgets('should not repeat repeatOnAuthError is set to false',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendGet(
          '${appConfig.serverUrl}/users/me', headers: anyNamed('headers')))
          .thenAnswer((_) async => unauthorizedResponse);

        try {
          await getUserData(repeatOnAuthError: false);
        } catch (exception) {
          expect(exception is UnauthorizedException, true,
            reason: 'incorrect exception type');
        }

        verify(http.sendGet(
          '${appConfig.serverUrl}/users/me', headers: anyNamed('headers')))
          .called(1);
      });
    });
  });
}


// DATA & helper functions


final _mockDate = DateTime(2010, 2, 26, 2, 14, 3);

final _mockUserData = {
  'id': 'id',
  'display_name': 'test user',
  'first_name': 'test',
  'last_name': 'user',
  'email': 'test@email.email',
  'picture_url': '',
  'created_at': _mockDate.toIso8601String(),
  'updated_at': _mockDate.toIso8601String(),
};

final _userJSON = jsonEncode(_mockUserData);
final _mockUser = User.fromJSON(_mockUserData);
