import 'package:flutter_test/flutter_test.dart';

import 'package:quanti/water/water_model.dart';


void main() {
  group('"WaterEntriesState"', () {
    test('should initialize with an empty list and summary', () {
      final waterEntriesState = WaterEntriesState.initial();
      final list = waterEntriesState.list;
      final summary = waterEntriesState.summary;

      expect(list is List && list.isEmpty, true,
        reason: 'water entries list is not an empty List');
      expect(summary is WaterSummary &&
        summary.start == null &&
        summary.end == null &&
        summary.aggregates is List &&
        summary.aggregates.isEmpty,
        true,
        reason: 'water entries summary is not empty WaterSummary');
    });

    group('"update"', () {
      test('should return new, updated WaterEntriesState', () {
        final initialState = WaterEntriesState.initial();

        final List<WaterEntry> newList = [];
        final newSummary = WaterSummary.initial();

        final updated = initialState.update(newList: newList, newSummary: newSummary);

        expect(updated == initialState, false,
          reason: 'new state is not a new instance');
        expect(updated.list, newList,
          reason: 'new list is the same as provided');
        expect(updated.summary, newSummary,
          reason: 'new summary is the same as provided');
      });
    });
  });

  group('"parseAggregates"', () {
    test('should parse dynamic list into list of WaterAggregates', () {
      final data = [
        {
          'volume': 200,
          'timestamp': '2021-04-16T19:37:16+00:00'
        }
      ];

      final result = parseAggregates(data);
      final element = result.first;
      final expectedDate = DateTime(2021, 4, 16, 19, 37, 16);

      expect(result is List<WaterAggregate>, true, reason: 'incorrect type');
      expect(element is WaterAggregate, true, reason: 'element is not a WaterAggregate');
      expect(element.volume, 200, reason: 'aggregate volume is incorrect');
      expect(element.timestamp is DateTime, true,
        reason: 'aggregate timestamp is incorrect');
      compareDates(element.timestamp, expectedDate);
    });
  });

  group('"WaterSummary"', () {

    group('"initial"', () {
      test('should be initialized with empty values', () {
        final summary = WaterSummary.initial();

        expect(summary.end, null, reason: 'end time not null');
        expect(summary.start, null, reason: 'start time not null');
        expect(summary.aggregates is List && summary.aggregates.isEmpty, true,
          reason: 'aggregates are not an empty list');
      });
    });

    group('"fromJSON"', () {
      test('should parse dynamic data to WaterSummary object', () {
        final data = {
          'start': '2021-04-14T14:01:05+00:00',
          'end': '2021-04-17T19:37:16+00:00',
          'aggregates': [
            {
              'volume': 200,
              'timestamp': '2021-04-16T19:37:16+00:00'
            }
          ]
        };

        final parsed = WaterSummary.fromJSON(data);
        final expectedStart = DateTime(2021, 4, 14, 14, 1, 5);
        final expectedEnd = DateTime(2021, 4, 17, 19, 37, 16);

        compareDates(parsed.start, expectedStart);
        compareDates(parsed.end, expectedEnd);
        expect(parsed.aggregates is List && parsed.aggregates.first is WaterAggregate,
          true,
          reason: 'parsed aggregates are not a list of WaterAggregates');
      });
    });
  });

  group('"WaterEntry"', () {
    test('should initialize with provided parameters', () {
      final dateTime = DateTime.now();
      final id = '123';
      final volume = 100;
      final entry = WaterEntry(id, volume, dateTime, dateTime, dateTime);

      expect(entry.id, id, reason: 'id is incorrect');
      expect(entry.volume, volume, reason: 'volume is incorrect');
      expect(entry.createdAt, dateTime, reason: 'createdAt is incorrect');
      expect(entry.updatedAt, dateTime, reason: 'updatedAt is incorrect');
      expect(entry.timestamp, dateTime, reason: 'timestamp is incorrect');
    });

    group('"fromJSON"', () {
      test('should parse dynamic map to WaterEntry object', () {
        final id = 'id';
        final volume = 300;
        final date = DateTime(2021, 1, 14, 12, 13, 14);
        final data = {
          'id': id,
          'volume': volume,
          'created_at': date.toIso8601String(),
          'updated_at': date.toIso8601String(),
          'timestamp': date.toIso8601String(),
        };

        final entry = WaterEntry.fromJSON(data);

        expect(entry.id, id, reason: 'id is incorrect');
        expect(entry.volume, volume, reason: 'volume is incorrect');
        expect(entry.timestamp, date, reason: 'timestamp is incorrect');
        expect(entry.createdAt, date, reason: 'createdAt is incorrect');
        expect(entry.updatedAt, date, reason: 'updatedAt is incorrect');
      });
    });

    group('"toJSON"', () {
      test('should JSON encode editable WaterEntry fields', () {
        final date = DateTime(2021,3,1,11,12,14);
        final entry = WaterEntry('id', 100, date, date, date);
        final encoded = entry.toJSON();
        final expected = '{"volume":100,"timestamp":"2021-03-01T11:12:14.000"}';

        expect(encoded, expected, reason: 'incorrect JSON format');
      });
    });
  });
}


// DATA & helper functions


void compareDates(actualDate, expectedDate) {
  expect(actualDate.year == expectedDate.year &&
    actualDate.month == expectedDate.month &&
    actualDate.day == expectedDate.day &&
    actualDate.hour == expectedDate.hour &&
    actualDate.minute == expectedDate.minute &&
    actualDate.second == expectedDate.second,
    true,
    reason: 'date is incorrect');
}
