import 'package:flutter_test/flutter_test.dart';

import 'package:quanti/water/water_utils.dart';


void main() {
  group('"IconConfig"', () {
    test('should initialize with provided values', () {
      IconConfig iconConfig = IconConfig(10, 15, 20, 25, '100 ml', 30, 100);

      expect(iconConfig.paddingLeft, 10, reason: 'paddingLeft is incorrect');
      expect(iconConfig.paddingRight, 15, reason: 'paddingRight is incorrect');
      expect(iconConfig.iconSize, 20, reason: 'iconSize is incorrect');
      expect(iconConfig.fontSize, 25, reason: 'fontSize is incorrect');
      expect(iconConfig.text, '100 ml', reason: 'text is incorrect');
      expect(iconConfig.textPadding, 30, reason: 'textPadding is incorrect');
      expect(iconConfig.volume, 100, reason: 'volume is incorrect');
    });

    group('"setPaddingRight"', () {
      test('should change only paddingRight property', () {
        IconConfig iconConfig = IconConfig(10, 15, 20, 25, '100 ml', 30, 100);

        IconConfig result = iconConfig.setPaddingRight(35);

        expect(result.paddingLeft, iconConfig.paddingLeft,
          reason: 'paddingLeft is incorrect');
        expect(result.paddingRight, 35, reason: 'paddingRight is incorrect');
        expect(result.iconSize, iconConfig.iconSize,
          reason: 'iconSize is incorrect');
        expect(result.fontSize, iconConfig.fontSize,
          reason: 'fontSize is incorrect');
        expect(result.text, iconConfig.text, reason: 'text is incorrect');
        expect(result.textPadding, iconConfig.textPadding,
          reason: 'textPadding is incorrect');
        expect(result.volume, iconConfig.volume, reason: 'volume is incorrect');
      });
    });
  });

  group('"waterIconConfigs"', () {
    test('should contain 3 proper icon configs', () {
      waterIconConfigs.keys.forEach((key) {
        expect(key is int, true, reason: 'key(volume) is not an integer');
        expect(waterIconConfigs[key] is IconConfig, true,
          reason: 'value should be an IconConfig');
      });
    });
  });
}
