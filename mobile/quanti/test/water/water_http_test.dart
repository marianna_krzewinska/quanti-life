import 'dart:convert';
import 'dart:io';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';

import 'package:quanti/auth.dart';
import 'package:quanti/configs/index.dart';
import 'package:quanti/http.dart';
import 'package:quanti/water/water_http.dart';
import 'package:quanti/water/water_model.dart';

import '../utils.dart';


void main() {
  group('"getWaterEntries"', () {
    testWidgets('should return a list of water entries', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendGet(
          '${appConfig.serverUrl}/entries/water', headers: anyNamed('headers')))
          .thenAnswer((_) async => Response(_entriesListJSON, HttpStatus.ok));

        final result = await getWaterEntries();

        verify(http.sendGet(
          '${appConfig.serverUrl}/entries/water', headers: anyNamed('headers')))
          .called(1);

        expect(result is List, true, reason: 'wrong type of returned data');
        expect(result.first is WaterEntry, true, reason: 'wrong type of list item');
        expect(result.first.id, _mockEntriesList.first['id'],
          reason: 'incorrect data returned');
      });
    });

    testWidgets('should retry on unathorized error', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        int execs = 0;

        when(http.sendGet(
          '${appConfig.serverUrl}/entries/water', headers: anyNamed('headers')))
          .thenAnswer((_) async {
            execs += 1;

            if (execs == 1) {
              return unauthorizedResponse;
            }

            return Response(_entriesListJSON, HttpStatus.ok);
          });

        final result = await getWaterEntries();

        verify(http.sendGet(
          '${appConfig.serverUrl}/entries/water', headers: anyNamed('headers')))
          .called(2);

        expect(result is List, true, reason: 'wrong type of returned data');
        expect(result.first is WaterEntry, true, reason: 'wrong type of list item');
        expect(result.first.id, _mockEntriesList.first['id'],
          reason: 'incorrect data returned');
      });
    });

    testWidgets('should not retry if repeatOnAuthError set to false',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendGet(
          '${appConfig.serverUrl}/entries/water', headers: anyNamed('headers')))
          .thenAnswer((_) async => unauthorizedResponse);

        try {
          await getWaterEntries(repeatOnAuthError: false);
        } catch (exception) {
          expect(exception is UnauthorizedException, true,
            reason: 'thrown exception is not UnauthorizedException');
        }

        verify(http.sendGet(
          '${appConfig.serverUrl}/entries/water', headers: anyNamed('headers')))
          .called(1);
      });
    });
  });

  group('"getWaterEntriesSummary"', () {
    testWidgets('should return water summary', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendGet(
          startsWith('${appConfig.serverUrl}/entries/water/summary'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async => Response(_entriesSummaryJSON, HttpStatus.ok));

        final result = await getWaterEntriesSummary(_mockDate, _mockDate);

        verify(http.sendGet(any, headers: anyNamed('headers'))).called(1);

        expect(result is WaterSummary, true, reason: 'wrong type of returned data');
        expect(result.aggregates is List
          && result.aggregates.first is WaterAggregate,
          true,
          reason: 'wrong type of aggregates');
        expect(result.start is DateTime && result.end is DateTime,
          true,
          reason: 'wrong type of start and end times');
      });
    });

    testWidgets('should repeat on unauthorized error', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        int execs = 0;

        when(http.sendGet(
          startsWith('${appConfig.serverUrl}/entries/water/summary'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async {
            execs += 1;

            if (execs == 1) {
              return unauthorizedResponse;
            }

            return Response(_entriesSummaryJSON, HttpStatus.ok);
          });

        final result = await getWaterEntriesSummary(_mockDate, _mockDate);

        verify(http.sendGet(
          startsWith('${appConfig.serverUrl}/entries/water/summary'),
          headers: anyNamed('headers'))).called(2);

        expect(result is WaterSummary, true, reason: 'wrong type of returned data');
        expect(result.aggregates is List
          && result.aggregates.first is WaterAggregate,
          true,
          reason: 'wrong type of aggregates');
        expect(result.start is DateTime && result.end is DateTime,
          true,
          reason: 'wrong type of start and end times');
      });
    });

    testWidgets('should not repeat if repeatOnAuthError is set to false',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendGet(
          startsWith('${appConfig.serverUrl}/entries/water/summary'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async => unauthorizedResponse);

        try {
          await getWaterEntriesSummary(_mockDate, _mockDate, repeatOnAuthError: false);
        } catch (exception) {
          expect(exception is UnauthorizedException, true,
            reason: 'incorrect exception thrown');
        }

        verify(http.sendGet(
          startsWith('${appConfig.serverUrl}/entries/water/summary'),
          headers: anyNamed('headers'))).called(1);
      });
    });
  });

  group('"updateWaterEntry"', () {
    testWidgets('should return updated water entry', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendPut(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async => Response(_entryJSON, HttpStatus.ok));

        final result = await updateWaterEntry(
          _mockEntry.id,
          _mockEntry.volume,
          _mockEntry.timestamp);

        verify(http.sendPut(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers'))).called(1);

        expect(result is WaterEntry, true, reason: 'wrong type of returned data');
        expect(result.id, _mockEntry.id, reason: 'incorrect id');
        expect(result.volume, _mockEntry.volume, reason: 'incorrect volume');
        expect(result.timestamp, _mockEntry.timestamp, reason: 'incorrect timestamp');
      });
    });

    testWidgets('should retry on unauthorized', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        int execs = 0;

        when(http.sendPut(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async {
            execs += 1;

            if (execs == 1) { return unauthorizedResponse; }

            return Response(_entryJSON, HttpStatus.ok);
          });

        final result = await updateWaterEntry(
          _mockEntry.id,
          _mockEntry.volume,
          _mockEntry.timestamp);

        verify(http.sendPut(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers'))).called(2);

        expect(result is WaterEntry, true, reason: 'wrong type of returned data');
        expect(result.id, _mockEntry.id, reason: 'incorrect id');
        expect(result.volume, _mockEntry.volume, reason: 'incorrect volume');
        expect(result.timestamp, _mockEntry.timestamp, reason: 'incorrect timestamp');
      });
    });

    testWidgets('should not retry if repeatOnAuthError set to false',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendPut(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async => unauthorizedResponse);

        try {
          await updateWaterEntry(
            _mockEntry.id,
            _mockEntry.volume,
            _mockEntry.timestamp,
            repeatOnAuthError: false);
        } catch (exception) {
          expect(exception is UnauthorizedException, true,
            reason: 'incorrect exception type');
        }

        verify(http.sendPut(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers'))).called(1);
      });
    });
  });

  group('"deleteWaterEntry"', () {
    testWidgets('should return id if successful', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendDelete(
          startsWith('${appConfig.serverUrl}/entries/water'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async => Response('1', HttpStatus.ok));

        final result = await deleteWaterEntry(_mockEntry.id);

        verify(http.sendDelete(
          startsWith('${appConfig.serverUrl}/entries/water'),
          headers: anyNamed('headers'))).called(1);

        expect(result, _mockEntry.id, reason: 'wrong type of returned data');
      });
    });

    testWidgets('should repeat on auth error', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        int execs = 0;

        when(http.sendDelete(
          startsWith('${appConfig.serverUrl}/entries/water'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async {
            execs += 1;

            if (execs == 1) { return unauthorizedResponse; }

            return Response('1', HttpStatus.ok);
          });

        final result = await deleteWaterEntry(_mockEntry.id);

        verify(http.sendDelete(
          startsWith('${appConfig.serverUrl}/entries/water'),
          headers: anyNamed('headers'))).called(2);

        expect(result, _mockEntry.id, reason: 'wrong type of returned data');
      });
    });

    testWidgets('should not repeat if repeatOnAuthError set to false',
      (WidgetTester tester) async {

      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendDelete(
          startsWith('${appConfig.serverUrl}/entries/water'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async => unauthorizedResponse);

        try {
          await deleteWaterEntry(_mockEntry.id, repeatOnAuthError: false);
        } catch (exception) {
          expect(exception is UnauthorizedException, true,
            reason: 'icorrect exception type');
        }

        verify(http.sendDelete(
          startsWith('${appConfig.serverUrl}/entries/water'),
          headers: anyNamed('headers'))).called(1);
      });
    });
  });

  group('"createWaterEntry"', () {
    testWidgets('should return created water entry', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendPost(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async => Response(_entryJSON, HttpStatus.ok));

        final result = await createWaterEntry(
          _mockEntry.volume,
          _mockEntry.timestamp);

        verify(http.sendPost(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers'))).called(1);

        expect(result is WaterEntry, true, reason: 'wrong type of returned data');
        expect(result.id, _mockEntry.id, reason: 'incorrect id');
        expect(result.volume, _mockEntry.volume, reason: 'incorrect volume');
        expect(result.timestamp, _mockEntry.timestamp, reason: 'incorrect timestamp');
      });
    });

    testWidgets('should retry on auth error', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        int execs = 0;

        when(http.sendPost(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async {
            execs += 1;

            if (execs == 1) { return unauthorizedResponse; }

            return Response(_entryJSON, HttpStatus.ok);
          });

        final result = await createWaterEntry(
          _mockEntry.volume,
          _mockEntry.timestamp);

        verify(http.sendPost(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers'))).called(2);

        expect(result is WaterEntry, true, reason: 'wrong type of returned data');
        expect(result.id, _mockEntry.id, reason: 'incorrect id');
        expect(result.volume, _mockEntry.volume, reason: 'incorrect volume');
        expect(result.timestamp, _mockEntry.timestamp, reason: 'incorrect timestamp');
      });
    });

    testWidgets('should not retry if repeatOnAuthError is set to false',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        when(http.sendPost(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers')))
          .thenAnswer((_) async => unauthorizedResponse);

        try {
          await createWaterEntry(
            _mockEntry.volume,
            _mockEntry.timestamp,
            repeatOnAuthError: false);
        } catch (exception) {
          expect(exception is UnauthorizedException, true,
            reason: 'incorrect exception type');
        }

        verify(http.sendPost(
          startsWith('${appConfig.serverUrl}/entries/water'),
          body: anyNamed('body'),
          headers: anyNamed('headers'))).called(1);
      });
    });
  });
}


// DATA & helper functions


final _mockDate = DateTime(2020, 9, 18, 21, 33, 16);

final _mockEntryData = {
  'id': 'id',
  'volume': 200,
  'created_at': _mockDate.toIso8601String(),
  'updated_at': _mockDate.toIso8601String(),
  'timestamp': _mockDate.toIso8601String(),
};

final _mockEntry = WaterEntry.fromJSON(_mockEntryData);

final _mockEntriesList = [_mockEntryData];

final _mockEntriesSummary = {
  'start': _mockDate.toIso8601String(),
  'end': _mockDate.toIso8601String(),
  'aggregates': [{
    'timestamp': _mockDate.toIso8601String(),
    'volume': 400,
  }]
};

final _entriesSummaryJSON = jsonEncode(_mockEntriesSummary);
final _entryJSON = jsonEncode(_mockEntryData);
final _entriesListJSON = jsonEncode(_mockEntriesList);
