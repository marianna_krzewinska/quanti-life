import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/utils.dart';
import 'package:quanti/water/water_model.dart';
import 'package:quanti/water/widgets/water_edit_dialog.dart';

import '../../utils.dart';


void main() {
  group('"WaterEditDialog"', () {
    testWidgets('should display all WaterEditDialog components',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeWaterEditDialog(tester);

        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.byType(WaterEditDialog), findsOneWidget,
          reason: 'dialog not opened');

        expect(find.text('Wed, Apr 15'), findsOneWidget,
          reason: 'displayed date is incorrect');
        expect(find.text('07:14'), findsOneWidget,
          reason: 'displayed time is incorrect');

        expect(find.byIcon(Icons.local_drink), findsNWidgets(3),
          reason: 'did not show 3 add water icons');

        for(int volume in [100, 200, 300]) {
          expect(find.text('$volume ml'), findsOneWidget,
            reason: '$volume ml text not displayed');
        }

        expect(find.byIcon(Icons.delete_outline), findsOneWidget,
          reason: 'did not find delete icon');
        expect(find.byIcon(Icons.done), findsOneWidget,
          reason: 'did not find save icon');
      });
    });

    testWidgets('should allow date change after date tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeWaterEditDialog(tester);

        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.byType(WaterEditDialog), findsOneWidget,
          reason: 'dialog not opened');

        await tester.tap(find.text('Wed, Apr 15'));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.text('SELECT DATE'), findsOneWidget,
          reason: 'date picker not found');

        await tester.tap(find.text('1'));
        await tester.tap(find.text('OK'));
        await tester.pumpAndSettle(Duration(seconds: 2));

        final _newDate = formatDate(DateTime(_date.year, _date.month, 1));

        expect(find.text(_newDate), findsOneWidget,
          reason: 'incorrect date after change');
      });
    });

    testWidgets('should allow time change after time tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeWaterEditDialog(tester);

        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.byType(WaterEditDialog), findsOneWidget,
          reason: 'dialog not opened');

        await tester.tap(find.text('07:14'));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.text('SELECT TIME'), findsOneWidget,
          reason: 'time picker not found');

        final _center = tester.getCenter(find
          .byKey(const ValueKey<String>('time-picker-dial')));
        await tester.tapAt(Offset(_center.dx, _center.dy + 50.0));

        await tester.tap(find.text('OK'));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.text('06:14'), findsOneWidget,
          reason: 'incorrect time after change');
      });
    });

    testWidgets('should close the dialog on save tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeWaterEditDialog(tester);

        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.byType(WaterEditDialog), findsOneWidget,
          reason: 'dialog not opened');

        await tester.tap(find.byIcon(Icons.done));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.byType(WaterEditDialog), findsNothing,
          reason: 'dialog was not closed');
      });
    });

    testWidgets('should close the dialog on delete tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeWaterEditDialog(tester);

        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.byType(WaterEditDialog), findsOneWidget,
          reason: 'dialog not opened');

        await tester.tap(find.byIcon(Icons.delete_outline));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.byType(WaterEditDialog), findsNothing,
          reason: 'dialog was not closed');
      });
    });
  });
}


// DATA & helper functions


final _date = DateTime(2020, 4, 15, 7, 14, 0);
final _entry = WaterEntry('id', 200, _date, _date, _date);

class ShowDialog extends StatelessWidget {
  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: Text('Open water edit dialog')),
        body: GestureDetector(
          child: Card(child: Text('Open dialog')),
          onTap: () => showDialog(
            context: context,
            builder: (BuildContext context) => WaterEditDialog(waterEntry: _entry)
          ),
        ),
      );
    }
}

Future initializeWaterEditDialog(WidgetTester tester) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
    middleware: [
      NavigationMiddleware<AppState>(),
    ]
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      navigatorKey: NavigatorHolder.navigatorKey,
      routes: {
        '/': (_) => ShowDialog(),
      })),
    Duration(seconds: 1000),
  );
}
