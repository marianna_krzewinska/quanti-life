import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/water/water_utils.dart';
import 'package:quanti/water/widgets/water_sized_button.dart';

import '../../utils.dart';


void main() {
  group('"WaterSizedButton"', () {
    testWidgets('should display ml and water cup icon',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeWaterSizedButton(tester);

        final mlText = find.text('300 ml');
        final icon = find.byIcon(Icons.local_drink);

        expect(mlText, findsOneWidget, reason: 'ml text is incorrect');
        expect(icon, findsOneWidget, reason: 'icon is not displayed');
      });
    });
  });
}


// DATA & helper functions


Future initializeWaterSizedButton(WidgetTester tester) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => Card(
          child: WaterSizedButton(config: waterIconConfigs[300])
        ),
      })),
    Duration(seconds: 1000),
  );
}
