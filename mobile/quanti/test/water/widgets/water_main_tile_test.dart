import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/summary/summary_model.dart';
import 'package:quanti/user/user_state.dart';
import 'package:quanti/water/water_model.dart';
import 'package:quanti/water/widgets/water_add_button.dart';
import 'package:quanti/water/widgets/water_main_tile.dart';

import '../../utils.dart';

void main() {
  group('"WaterMainTile"', () {
    testWidgets('should show today\'s ml count and 3 add water buttons',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeMainTile(tester);

        final mlText = find.text('400 ml');
        final addWaterButtons = find.byType(AddWaterButton);

        expect(mlText, findsOneWidget,
          reason: 'daily ml are incorrectly displayed');
        expect(addWaterButtons, findsNWidgets(3),
          reason: 'did not find 3 add water buttons');
      });
    });

    testWidgets('should sum only water from today\'s entries',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeMainTile(tester, state: WaterEntriesState
          .initial()
          .update(newList: [
            WaterEntry.fromJSON(_todayEntryData),
            WaterEntry.fromJSON(_todayEntryData),
            WaterEntry.fromJSON(_yesterdayEntryData),
            WaterEntry.fromJSON(_yesterdayEntryData),
          ]));

        final mlText = find.text('400 ml');

        expect(mlText, findsOneWidget,
          reason: 'daily ml are incorrectly displayed');
      });
    });
  });
}


// DATA & helper functions


final _today = DateTime.now();
final _yesterday = _today.subtract(Duration(days: 1));

final _todayEntryData = {
  'id': 'id',
  'volume': 200,
  'created_at': _today.toIso8601String(),
  'updated_at': _today.toIso8601String(),
  'timestamp': _today.toIso8601String(),
};

final _yesterdayEntryData = {
  'id': 'id',
  'volume': 200,
  'created_at': _yesterday.toIso8601String(),
  'updated_at': _yesterday.toIso8601String(),
  'timestamp': _yesterday.toIso8601String(),
};


Future initializeMainTile(WidgetTester tester, { WaterEntriesState state }) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState(
      state ?? WaterEntriesState
        .initial()
        .update(newList: [
          WaterEntry.fromJSON(_todayEntryData),
          WaterEntry.fromJSON(_todayEntryData),
        ]),
      null,
      initialUserDataState,
      SummarySettings.initial(),
    ),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => WaterMainTile(),
      })),
    Duration(seconds: 1000),
  );
}
