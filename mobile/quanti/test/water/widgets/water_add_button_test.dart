import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/water/water_utils.dart';
import 'package:quanti/water/widgets/water_add_button.dart';
import 'package:quanti/water/widgets/water_sized_button.dart';

import '../../utils.dart';


void main() {
  group('"AddWaterButton"', () {
    testWidgets('should display WaterSizedButton',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeAddWaterButton(tester);

        final waterBtn = find.byType(WaterSizedButton);

        expect(waterBtn, findsOneWidget,
          reason: 'WaterSizedButton is not displayed');
      });
    });
  });
}


// DATA & helper functions


Future initializeAddWaterButton(WidgetTester tester) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => Card(
          child: AddWaterButton(config: waterIconConfigs[300])
        ),
      })),
    Duration(seconds: 1000),
  );
}
