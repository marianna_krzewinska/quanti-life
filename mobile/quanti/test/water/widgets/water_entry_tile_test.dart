import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/water/water_model.dart';
import 'package:quanti/water/widgets/water_edit_dialog.dart';
import 'package:quanti/water/widgets/water_entry_tile.dart';
import 'package:quanti/water/widgets/water_sized_button.dart';

import '../../utils.dart';


void main() {
  group('"WaterEntryTile"', () {
    testWidgets('should display timestamp and water cup icon with ml',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeWaterEntryTile(tester);

        final sizedBtn = find.byType(WaterSizedButton);
        final hour = find.text('07:14');

        expect(sizedBtn, findsOneWidget, reason: 'button is not displayed');
        expect(hour, findsOneWidget, reason: 'hour is incorrect');
      });
    });

    testWidgets('should open edit dialog on click',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeWaterEntryTile(tester);

        await tester.tap(find.byType(WaterEntryTile));
        await tester.pumpAndSettle(Duration(seconds: 1));

        final dialog = find.byType(WaterEditDialog);

        expect(dialog, findsOneWidget, reason: 'dialog is not displayed');
      });
    });
  });
}


// DATA & helper functions


final _today = DateTime(2020, 4, 15, 7, 14, 0);

final _entry = WaterEntry('id', 200, _today, _today, _today);


Future initializeWaterEntryTile(WidgetTester tester) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => WaterEntryTile(waterEntry: _entry),
      })),
    Duration(seconds: 1000),
  );
}
