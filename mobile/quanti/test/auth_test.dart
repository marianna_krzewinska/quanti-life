import 'dart:io';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:redux/redux.dart';

import 'package:quanti/auth.dart';
import 'package:quanti/google_sign_in.dart';
import 'package:quanti/main_state.dart';
import 'package:quanti/shared_preferences.dart';
import 'package:quanti/user/user_state.dart';

import 'utils.dart';


void main() {
  group('"AuthView"', () {
    testWidgets('"Should display a loading animation"', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeAuthView(tester);

        final spinner = find.byType(SpinKitFadingCube);

        expect(spinner, findsOneWidget, reason: 'spinner not found');
      });
    });
  });

  group('"createAuthHeader"', () {
    test('should return map with auth header', () {
      String token = 'test';
      final header = createAuthHeader(token);

      expect(header is Map, true, reason: 'return type is incorrect');
      expect(header['Authorization'], token, reason: 'object model is incorrect');
    });
  });

  group('"getAuthHeaders"', () {
    testWidgets('should return headers with token if token in storage',
      (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeSharedPreferences();
          when(sharedPreferences.getString(_authTokenKey)).thenReturn(mockAccessToken);

          Map<String, String> headers = await getAuthHeaders();

          expect(headers['Authorization'], mockAccessToken,
            reason: 'mocked token not returned');
        });
    });

    testWidgets('should return headers with token if token in storage',
      (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeSharedPreferences();
          when(sharedPreferences.getString(_authTokenKey)).thenReturn(null);
          when(googleSignIn.signInSilently()).thenAnswer((_) => Future.value(_mockAccount));

          Map<String, String> headers = await getAuthHeaders();

          expect(headers['Authorization'], mockAccessToken,
            reason: 'mocked token not returned');
        });
    });

    testWidgets('should return empty auth header if silent sign in fails',
      (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeSharedPreferences();
          when(sharedPreferences.getString(_authTokenKey)).thenReturn(null);
          when(googleSignIn.signInSilently()).thenAnswer((_) => Future.value(null));

          Map<String, String> headers = await getAuthHeaders();

          expect(headers['Authorization'], '',
            reason: 'auth header is not empty');
        });
    });
  });

  group('"resetAuthHeaders"', () {
    testWidgets('should return true if successfully reset auth token',
      (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeSharedPreferences();

          when(sharedPreferences.setString(_authTokenKey, null))
            .thenAnswer((_) => Future.value(true));

          bool result = await resetAuthHeaders();

          expect(result, true, reason: 'auth token reset failed');
        });
    });
  });

  group('"handleAuthError"', () {
    test('shuld return LogoutAction on UnauthorizedException', () {
      final exception = UnauthorizedException(message: 'test');
      final result = handleAuthError(exception);

      expect(result is LogoutAction, true, reason: 'didn\'t return LogoutAction');
    });
  });

  group('"handleHttpSuccessResponse"', () {
    test('should return a parsed body on ok', () {
      Response response = Response(
        '{"test":"test message","someNumber":1234}',
        HttpStatus.ok);
      final result = handleHttpSuccessResponse(response);

      expect(result is Map, true, reason: 'result is not a Map');
      expect(result['test'], 'test message', reason: 'parsed text is incorrect');
      expect(result['someNumber'], 1234, reason: 'parsed number is incorrect');
    });

    test('should return a parsed body on status created', () {
      Response response = Response(
        '{"test":"test message","someNumber":1234}',
        HttpStatus.created);
      final result = handleHttpSuccessResponse(response);

      expect(result is Map, true, reason: 'result is not a Map');
      expect(result['test'], 'test message', reason: 'parsed text is incorrect');
      expect(result['someNumber'], 1234, reason: 'parsed number is incorrect');
    });
  });

  group('"handleHttpErrorResponse"', () {
    test('should throw UnauthorizedException on unauthorized', () {
      Response response = Response(
        '',
        HttpStatus.unauthorized,
        reasonPhrase: 'unathorized');
        expect(() => handleHttpErrorResponse(response),
          throwsA(isA<UnauthorizedException>()),
          reason: 'UnauthorizedException was not thrown');
    });

    test('should throw UnauthorizedException on forbidden', () {
      Response response = Response(
        '',
        HttpStatus.forbidden,
        reasonPhrase: 'firbidden');
        expect(() => handleHttpErrorResponse(response),
          throwsA(isA<UnauthorizedException>()),
          reason: 'UnauthorizedException was not thrown');
    });

    test('should throw HttpCustomException on any other status code', () {
      Response response = Response(
        '',
        HttpStatus.serviceUnavailable, // or any other status code
        reasonPhrase: 'unathorized');
        expect(() => handleHttpErrorResponse(response),
          throwsA(isA<HttpCustomException>()),
          reason: 'HttpCustomException was not thrown');
    });
  });
}


// DATA & helper functions


String _authTokenKey = 'auth-token';
GoogleSignInAccount _mockAccount = MockGoogleSignInAccount();


Future initializeAuthView(WidgetTester tester) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: AuthView.route,
      routes: {
        AuthView.route: (_) => AuthView(),
      })),
    Duration(seconds: 1000),
  );
}
