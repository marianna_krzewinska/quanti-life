import 'package:flutter_test/flutter_test.dart';

import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/urine/urine_utils.dart';


void main() {
  group('"getUrineColorIdByCode"', () {
    test('should map urine color code to id', () {
      final id = getUrineColorIdByCode(defaultUrineColor.code);

      expect(id, defaultUrineColor.id, reason: 'incorrect id');
    });
  });

  group('"getUrineColorCodeById"', () {
    test('should map urine color id to code', () {
      final code = getUrineColorCodeById(defaultUrineColor.id);

      expect(code, defaultUrineColor.code, reason: 'incorrect code');
    });
  });
}
