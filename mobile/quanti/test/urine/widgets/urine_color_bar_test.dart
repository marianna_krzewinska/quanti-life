import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';

import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/urine/urine_utils.dart';
import 'package:quanti/urine/widgets/urine_color_bar.dart';


void main() {
  group('"getBarConfig"', () {
    test('should generate bar config list from a list of entries', () {
      final barConfigs = getBarConfig([_entry, _entry1]);

      barConfigs.forEach((config) {
        expect(config is BarConfig, true, reason: 'not a bar config');
      });
    });

    test('should contain one item for single color entries', () {
      final barConfigs = getBarConfig([_entry, _entry3]);

      expect(barConfigs.length, 1, reason: 'incorrect number of configs');
      expect(barConfigs.first.color, urineColors[_entry.colorId].color,
        reason: 'color is incorrect');
      expect(barConfigs.first.width, maxWidth, reason: 'incorrect widgth');
    });

    test('should divide max width between entries colors', () {
      final barConfigs = getBarConfig([_entry, _entry1, _entry2, _entry3]);
      final expectedWidths = {
        urineColors[1].color: 56,
        urineColors[2].color: 14,
        urineColors[3].color: 70,
      };

      expect(barConfigs.length, 3, reason: 'incorrect number of configs');
      barConfigs.forEach((config) {
        expect(config.width, expectedWidths[config.color],
          reason: 'incorrect width for color ${config.color.toString()}');
      });
    });
  });

  group('"UrineColorBar"', () {
    testWidgets('should display one color bar for same color entries',
      (WidgetTester tester) async {

      await tester.runAsync(() async {
        final entries = [_entry, _entry3];

        await initializeUrineColorBar(tester, entries);

        expect(find.byType(Container), findsOneWidget,
          reason: 'incorrect number of bars displayed');
      });
    });

    testWidgets('should display 3 color bars for 3 color entries',
      (WidgetTester tester) async {

      await tester.runAsync(() async {
        final entries = [_entry, _entry1, _entry2, _entry3];

        await initializeUrineColorBar(tester, entries);

        expect(find.byType(Container), findsNWidgets(3),
          reason: 'incorrect number of bars displayed');
      });
    });

    testWidgets('should display 0 color bars for empty entries list',
      (WidgetTester tester) async {

      await tester.runAsync(() async {
        await initializeUrineColorBar(tester, []);

        expect(find.byType(Container), findsOneWidget,
          reason: 'did not return an empty container');
      });
    });
  });
}


// DATA & helper functions


final _date = DateTime(2021, 4, 10, 12, 37, 58);

final _entry = UrineEntry('id1', 300, urineColors[1].id, _date, _date, _date);
final _entry1 = _entry.update(
  newVolume: 100,
  newColorId: urineColors[2].id,
  newTimestamp: _date.add(Duration(minutes: 57)));
final _entry2 = _entry.update(
  newVolume: 500,
  newColorId: urineColors[3].id,
  newTimestamp: _date.add(Duration(minutes: 126)));
final _entry3 = _entry.update(
  newVolume: 100,
  newColorId: urineColors[1].id,
  newTimestamp: _date.add(Duration(minutes: 393)));


Future initializeUrineColorBar(
  WidgetTester tester,
  List<UrineEntry> entries) async {

  await tester.pumpWidget(MaterialApp(
    title: 'Quanti.life',
    initialRoute: '/',
    routes: {
      '/': (_) => UrineColorBar(entries: entries),
    }),
    Duration(seconds: 1000),
  );
}
