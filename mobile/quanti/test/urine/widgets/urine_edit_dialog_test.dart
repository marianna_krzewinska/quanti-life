
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:quanti/main_state.dart';
import 'package:quanti/summary/summary_model.dart';
import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/urine/urine_utils.dart';
import 'package:quanti/urine/widgets/urine_edit_dialog.dart';
import 'package:quanti/user/user_state.dart';
import 'package:quanti/utils.dart';
import 'package:redux/redux.dart';


void main() {
  group('"UrineEditDialog"', () {
    testWidgets('should display all UrineEditDialog components',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeShowDialog(tester, entries: [_entry]);
        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.text('Sat, Apr 10'), findsOneWidget, reason: 'date not found');
        expect(find.text('12:37'), findsOneWidget, reason: 'time not found');

        expect(find.byIcon(UrineIcons.toilet), findsNWidgets(3),
          reason: 'toilet icons not found');
        [100, 300, 500].forEach((volume) {
          expect(find.text('$volume ml'), findsOneWidget,
            reason: '$volume volume text not found');
        });

        expect(find.text('Color'), findsOneWidget,
          reason: 'Color change label not found');
        expect(find.byType(Slider), findsOneWidget,
          reason: 'Color change slider not found');

        expect(find.byIcon(Icons.delete_outline), findsOneWidget,
          reason: 'Delete icon not found');
        expect(find.byIcon(Icons.done), findsOneWidget,
          reason: 'Done icon not found');
      });
    });

    testWidgets('should display close icon instead of delete when creating new',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeShowDialog(tester, entries: [_entry], newEntry: true);
        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.byIcon(Icons.close), findsOneWidget,
          reason: 'Close icon not found');
      });
    });

    testWidgets('should allow date change after date tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeShowDialog(tester);

        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.byType(UrineEditDialog), findsOneWidget,
          reason: 'dialog not opened');

        await tester.tap(find.text('Sat, Apr 10'));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.text('SELECT DATE'), findsOneWidget,
          reason: 'date picker not found');

        await tester.tap(find.text('1'));
        await tester.tap(find.text('OK'));
        await tester.pumpAndSettle(Duration(seconds: 2));

        final _newDate = formatDate(DateTime(_date.year, _date.month, 1));

        expect(find.text(_newDate), findsOneWidget,
          reason: 'incorrect date after change');
      });
    });

    testWidgets('should allow time change after time tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeShowDialog(tester);

        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.byType(UrineEditDialog), findsOneWidget,
          reason: 'dialog not opened');
        expect(find.text('12:37'), findsOneWidget, reason: 'time not found');

        await tester.tap(find.text('12:37'));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.text('SELECT TIME'), findsOneWidget,
          reason: 'time picker not found');

        final _center = tester.getCenter(find
          .byKey(const ValueKey<String>('time-picker-dial')));
        await tester.tapAt(Offset(_center.dx - 50, _center.dy));

        await tester.tap(find.text('OK'));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.text('21:37'), findsOneWidget,
          reason: 'incorrect time after change');
      });
    });

    testWidgets('should close the dialog on close tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeShowDialog(tester, entries: [_entry], newEntry: true);
        //  Close button tap
        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 2));

        await tester.tap(find.byIcon(Icons.close));
        await tester.pumpAndSettle(Duration(seconds: 2));
      });
    });

    testWidgets('should close the dialog on delete tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeShowDialog(tester, entries: [_entry], newEntry: false);

        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.byIcon(Icons.delete_outline), findsOneWidget);

        await tester.tap(find.byIcon(Icons.delete_outline));
        await tester.pumpAndSettle(Duration(seconds: 2));
      });
    });

    testWidgets('should close the dialog on save tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeShowDialog(tester, entries: [_entry], newEntry: true);

        await tester.tap(find.byType(Card));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.byIcon(Icons.done), findsOneWidget);

        await tester.tap(find.byIcon(Icons.done));
        await tester.pumpAndSettle(Duration(seconds: 2));
      });
    });
  });
}


// DATA & helper functions


final _date = DateTime(2021, 4, 10, 12, 37, 58);

final _entry = UrineEntry('id', 300, urineColors[1].id, _date, _date, _date);


class ShowDialog extends StatelessWidget {
  final bool createNew;

  ShowDialog({Key key, this.createNew}) : super(key: key);

  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: Text('Open water edit dialog')),
        body: GestureDetector(
          child: Card(child: Text('Open dialog')),
          onTap: () => showDialog(
            context: context,
            builder: (BuildContext context) => UrineEditDialog(
              urineEntry: _entry,
              createNew: createNew,
            ),
          ),
        ),
      );
    }
}

Future initializeShowDialog(
  WidgetTester tester,
  { List<UrineEntry> entries, bool newEntry = false }) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState(
      null,
      UrineEntriesState
        .initial()
        .update(newList: entries ?? []),
      initialUserDataState,
      SummarySettings.initial(),
    ),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => ShowDialog(createNew: newEntry),
      })),
    Duration(seconds: 1000),
  );
}
