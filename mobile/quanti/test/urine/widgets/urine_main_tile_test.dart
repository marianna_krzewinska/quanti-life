import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/summary/summary_model.dart';
import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/urine/urine_utils.dart';
import 'package:quanti/urine/widgets/urine_color_bar.dart';
import 'package:quanti/urine/widgets/urine_edit_dialog.dart';
import 'package:quanti/urine/widgets/urine_main_tile.dart';
import 'package:quanti/user/user_state.dart';


void main() {
  group('"UrineMainTile"', () {
    testWidgets('should display volume, color bar and log urine icons',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        final entries = [_entry, _entry1];
        await initializeMainTile(tester, entries: entries);

        expect(find.text('0 ml'), findsOneWidget, reason: 'incorrect ml volume');
        expect(find.byType(UrineColorBar), findsOneWidget,
          reason: 'color bar not displayed');
        expect(find.byIcon(UrineIcons.toilet), findsOneWidget,
          reason: 'log urine icon not displayed');
      });
    });

    testWidgets('should display 0 ml if no entry from today',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        final entries = [_entry, _entry1];
        await initializeMainTile(tester, entries: entries);

        expect(find.text('0 ml'), findsOneWidget, reason: 'incorrect ml volume');
      });
    });

    testWidgets('should display correct ml volume of today\'s entries',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        final entries = [_entry.update(newTimestamp: DateTime.now()), _entry1];
        await initializeMainTile(tester, entries: entries);

        expect(find.text('300 ml'), findsOneWidget, reason: 'incorrect ml volume');
      });
    });

    testWidgets('should display correct ml volume of today\'s entries',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        final entries = [_entry.update(newTimestamp: DateTime.now()), _entry1];
        await initializeMainTile(tester, entries: entries);

        expect(find.text('300 ml'), findsOneWidget, reason: 'incorrect ml volume');
      });
    });

    testWidgets('should open UrineEditDialog on toilet icon tap',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        final entries = [_entry.update(newTimestamp: DateTime.now()), _entry1];
        await initializeMainTile(tester, entries: entries);

        await tester.tap(find.byIcon(UrineIcons.toilet));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.byType(UrineEditDialog), findsOneWidget,
          reason: 'dialog did not open');
      });
    });
  });

  group('"getUrineMl"', () {
    test('should return sum of volume as "\d+ ml" regardless of timestamp', () {
      final mlText = getUrineMl([_entry, _entry1]);

      expect(mlText, '400 ml', reason: 'incorrect ml volume text');
    });
  });

  group('"getTodaysEntries"', () {
    test('should filter out entries that are not from today', () {
      final todayEntry = _entry.update(newTimestamp: DateTime.now());

      final entries = [todayEntry, _entry, _entry1];
      final todayEntries = getTodaysEntries(entries);

      expect(todayEntries.length, 1, reason: 'does not contain just one entry');
      expect(todayEntries.first, todayEntry, reason: 'did not return today entry');
    });
  });
}


// DATA & helper functions


final _date = DateTime(2021, 4, 10, 12, 37, 58);

final _entry = UrineEntry('id', 300, urineColors[1].id, _date, _date, _date);
final _entry1 = _entry.update(
  newVolume: 100,
  newColorId: urineColors[2].id,
  newTimestamp: _date.add(Duration(days: 5)));


Future initializeMainTile(WidgetTester tester, { List<UrineEntry> entries }) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState(
      null,
      UrineEntriesState
        .initial()
        .update(newList: entries),
      initialUserDataState,
      SummarySettings.initial(),
    ),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => UrineMainTile(),
      })),
    Duration(seconds: 1000),
  );
}
