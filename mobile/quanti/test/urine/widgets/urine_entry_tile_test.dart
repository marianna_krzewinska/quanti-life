import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/urine/urine_utils.dart';
import 'package:quanti/urine/widgets/urine_edit_dialog.dart';
import 'package:quanti/urine/widgets/urine_entry_tile.dart';

import '../../utils.dart';

void main() {
  group('"UrineEntryTile"', () {
    testWidgets('should display ml and urine droplet',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeUrineEntryTile(tester);

        expect(find.byIcon(UrineIcons.toilet), findsOneWidget,
          reason: 'toilet icon not displayed');
        expect(find.text('200 ml'), findsOneWidget,
          reason: 'urine ml text is not displayed');
        expect(find.text('07:14'), findsOneWidget, reason: 'hour is incorrect');
      });
    });

    testWidgets('should open edit dialog on click',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeUrineEntryTile(tester);

        await tester.tap(find.byType(UrineEntryTile));
        await tester.pumpAndSettle(Duration(seconds: 1));

        final dialog = find.byType(UrineEditDialog);

        expect(dialog, findsOneWidget, reason: 'dialog is not displayed');
      });
    });
  });
}


// DATA & helper functions


final _date = DateTime(2020, 4, 15, 7, 14, 0);

final _entry = UrineEntry('id', 200, urineColors[1].id, _date, _date, _date);


Future initializeUrineEntryTile(WidgetTester tester) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => UrineEntryTile(entry: _entry),
      })),
    Duration(seconds: 1000),
  );
}
