import 'package:flutter_test/flutter_test.dart';

import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/urine/urine_utils.dart';

void main() {
  group('"UrineEntriesState"', () {
    test('should be initialized with provided values', () {
      final state = UrineEntriesState([_mockEntry], _mockSummary);

      expect(state.list is List, true, reason: 'entries list is not a List');
      expect(state.list.first is UrineEntry, true,
        reason: 'entry is not a UrineEntry');
      expect(state.summary is UrineSummary, true,
        reason: 'summary is not of type UrineSummary');
    });

    group('"initial"', () {
      test('should return empty state object', () {
        final state = UrineEntriesState.initial();

        expect(state.summary is UrineSummary, true,
          reason: 'summary has incorrect type');
        expect(state.list is List && state.list.isEmpty, true,
          reason: 'entries list is not an empty list');
      });
    });

    group('"update"', () {
      test('should return new object instance with updated fields', () {
        final state = UrineEntriesState.initial();
        final updated = state.update(newList: [_mockEntry], newSummary: _mockSummary);

        expect(updated.summary == state.summary, false,
          reason: 'summary did not change');
        expect(updated.list == state.list, false, reason: 'list did not change');
        expect(updated.list.first, _mockEntry, reason: 'list has incorrect value');
      });
    });
  });

  group('"UrineAggregate"', () {
    test('should be initialized with provided values', () {
      final aggregate = UrineAggregate(300, _mockDate, 2);

      expect(aggregate.volume, 300, reason: 'incorrect volume');
      expect(aggregate.timestamp, _mockDate, reason: 'incorrect timestamp');
      expect(aggregate.colorId, 2, reason: 'incorrect color id');
    });

    group('"parseJSON"', () {
      test('should parse dynamic List to List of UrineAggregates grouped by color',
        () {
        final parsed = UrineAggregate.parseJSON([_mockEntryAggregateData]);

        expect(parsed is List &&
          parsed.first is List &&
          parsed.first.first is UrineAggregate, true,
          reason: 'list of UrineAggregates was not returned');
        expect(parsed.first.first.volume, _mockEntryAggregateData['volume'],
          reason: 'parsed aggregate is incorrect');
      });
    });
  });

  group('"UrineSummary"', () {
    test('should be initialized with provided values', () {
      final summary = UrineSummary(
        aggregates: _mockAggregates,
        start: _mockDate,
        end: _mockDate,
      );

      expect(summary.aggregates, _mockAggregates,
        reason: 'aggregates value is incorrect');
      expect(summary.start, _mockDate, reason: 'start date is incorrect');
      expect(summary.end, _mockDate, reason: 'end date is incorrect');
    });

    group('"initial"', () {
      test('should return empty summary object', () {
        final summary = UrineSummary.initial();

        expect(summary.start, null, reason: 'start date is not null');
        expect(summary.end, null, reason: 'end date is not null');
        expect(summary.aggregates is List && summary.aggregates.isEmpty, true,
          reason: 'aggregates are not an empty list');
      });
    });

    group('"fromJSON"', () {
      test('should parse dynamic data to UrineSummary object', () {
        final summary = UrineSummary.fromJSON(_mockSummaryData);

        expect(summary.start is DateTime, true, reason: 'icorrect start date format');
        expect(summary.end is DateTime, true, reason: 'icorrect end date format');
        expect(summary.aggregates is List &&
          summary.aggregates.first is List &&
          summary.aggregates.first.first is UrineAggregate, true,
          reason: 'incorrect aggregates format');
      });
    });
  });

  group('"UrineEntry"', () {
    group('"create"', () {
      test('should create an UrineEntry object with given volume', () {
        final entry = UrineEntry.create(200);

        expect(entry.volume, 200, reason: 'incorrect volume');
        expect(entry.id, '', reason: 'incorrect id');
        expect(entry.colorId, defaultUrineColor.id, reason: 'incorrect color id');
      });
    });

    group('"fromJSON"', () {
      test('should parse dynamic data into UrineEntry object', () {
        final entry = UrineEntry.fromJSON(_mockEntryData);

        expect(entry.id, 'id', reason: 'incorrect id');
        expect(entry.volume, 300, reason: 'incorrect volume');
        expect(entry.colorId, urineColors[3].id, reason: 'incorrect colorId');
        expect(entry.timestamp is DateTime, true, reason: 'incorrect timestamp');
        expect(entry.createdAt is DateTime, true, reason: 'incorrect createdAt');
        expect(entry.updatedAt is DateTime, true, reason: 'incorrect updatedAt');
      });
    });

    group('"update"', () {
      test('should return new instance of UrineEntry with changed values', () {
        final entry = UrineEntry.create(300);
        final updated = entry.update(
          newVolume: 500,
          newColorId: urineColors[2].id,
          newTimestamp: _mockDate);

        expect(entry == updated, false, reason: 'did not return new instance');
        expect(updated.volume, 500, reason: 'incorrect volume');
        expect(updated.colorId, urineColors[2].id, reason: 'incorrect colorId');
        expect(updated.timestamp, _mockDate, reason: 'incorrect timestamp');
      });
    });

    group('"toJSON"', () {
      test('should generate an encoded JSON string from UrineEntry', () {
        final entry = UrineEntry('id', 300, urineColors[2].id, _mockDate, _mockDate, _mockDate);
        final expected = '{"volume":300,"timestamp":"2018-12-06T18:37:56.000","color":"255,255,235,59"}';
        final actual = entry.toJSON();

        expect(actual, expected, reason: 'incorrect JSON UrineEntry data');
      });
    });
  });
}


// DATA & helper functions


final _mockDate = DateTime(2018, 12, 6, 18, 37, 56);

final _mockEntryData = {
  'id': 'id',
  'volume': 300,
  'color': urineColors[3].code,
  'timestamp': _mockDate.toIso8601String(),
  'created_at': _mockDate.toIso8601String(),
  'updated_at': _mockDate.toIso8601String(),
};
final _mockEntry = UrineEntry.fromJSON(_mockEntryData);


final _mockEntryAggregateData = {
  'timestamp': _mockDate.toIso8601String(),
  'volume': 500,
  'color': urineColors[1].code,
};
final _mockAggregates = UrineAggregate.parseJSON([_mockEntryAggregateData]);


final _mockSummaryData = {
  'start': _mockDate.toIso8601String(),
  'end': _mockDate.toIso8601String(),
  'aggregates': [_mockEntryAggregateData],
};
final _mockSummary = UrineSummary.fromJSON(_mockSummaryData);
