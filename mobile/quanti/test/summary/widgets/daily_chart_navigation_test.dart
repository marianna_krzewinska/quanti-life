import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/summary/summary_model.dart';
import 'package:quanti/summary/widgets/daily_chart/daily_chart_navigation.dart';
import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/user/user_model.dart';
import 'package:quanti/utils.dart';
import 'package:quanti/water/water_model.dart';

import '../../utils.dart';


void main() {
  group('"DailyChartNavigation"', () {
    testWidgets('should display current date and buttons prev and next',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();
        final _today = DateTime.now();

        await initializeDailyChartNavigation(tester, _today);

        final _dateText = find.text(formatDate(_today));
        final _iconBtns = find.byType(IconButton);
        final _prevIcon = find.byIcon(Icons.keyboard_arrow_left);
        final _nextIcon = find.byIcon(Icons.keyboard_arrow_right);

        expect(_dateText, findsOneWidget, reason: 'date is incorrect');
        expect(_iconBtns, findsNWidgets(2), reason: 'icon buttons not found');
        expect(_prevIcon, findsOneWidget, reason: 'go to previous btn not found');
        expect(_nextIcon, findsOneWidget, reason: 'go to next btn not found');
      });
    });

    testWidgets('when today - clicking previous should change date to yesterday',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();
        final _today = DateTime.now();

        await initializeDailyChartNavigation(tester, _today);

        final _todayText = find.text(formatDate(_today));

        expect(_todayText, findsOneWidget, reason: 'today\'s date is incorrect');

        await tester.tap(find.byIcon(Icons.keyboard_arrow_left));
        await tester.pumpAndSettle(Duration(seconds: 1));

        final _yesterdayText = formatDate(_today
          .subtract(Duration(days: 1)));

        expect(find.text(_yesterdayText), findsOneWidget,
          reason: 'yesterday\'s date is incorrect');
      });
    });

    testWidgets('when today - clicking next should not change the date',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();
        final _today = DateTime.now();

        await initializeDailyChartNavigation(tester, _today);

        final _todayText = formatDate(_today);

        expect(find.text(_todayText), findsOneWidget,
          reason: 'today\'s date is incorrect');

        await tester.tap(find.byIcon(Icons.keyboard_arrow_right));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.text(_todayText), findsOneWidget,
          reason: 'date has changed');
      });
    });

    testWidgets('when yesterday - clicking next should change the date to today',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();
        final _yesterday = DateTime.now();

        await initializeDailyChartNavigation(tester, _yesterday);

        final _yesterdayText = formatDate(_yesterday);

        expect(find.text(_yesterdayText), findsOneWidget,
          reason: 'yesterday\'s date is incorrect');

        await tester.tap(find.byIcon(Icons.keyboard_arrow_right));
        await tester.pumpAndSettle(Duration(seconds: 1));

        expect(find.text(formatDate(DateTime.now())), findsOneWidget,
          reason: 'today\'s date is incorrect');
      });
    });
  });
}


// DATA & helper functions


Future initializeDailyChartNavigation(WidgetTester tester, DateTime selectedDate)
  async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState(
      WaterEntriesState.initial(),
      UrineEntriesState.initial(),
      UserData.initialize(),
      SummarySettings(
        selectedDate,
        null,
      ),
    ),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => Card(
          child: DailyChartNavigation()
        ),
      })),
    Duration(seconds: 1000),
  );
}
