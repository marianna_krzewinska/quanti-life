import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/main_state.dart';
import 'package:quanti/summary/summary_model.dart';
import 'package:quanti/summary/widgets/weekly_chart/weekly_chart_navigation.dart';
import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/user/user_model.dart';
import 'package:quanti/water/water_model.dart';

import '../../utils.dart';


void main() {
  group('"WeeklyChartNavigation"', () {
    testWidgets('should display date range and buttons prev and next',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();
        final _date = DateTime(2020, 4, 17);

        await initializeWeeklyChartNavigation(tester, SelectedPeriod(
          _date.subtract(Duration(days: 3)),
          _date.add(Duration(days: 3)),
        ));

        final _dateRange = find.text('Apr 14 - 20');
        final _iconBtns = find.byType(IconButton);
        final _prevIcon = find.byIcon(Icons.keyboard_arrow_left);
        final _nextIcon = find.byIcon(Icons.keyboard_arrow_right);

        expect(_dateRange, findsOneWidget, reason: 'date range is incorrect');
        expect(_iconBtns, findsNWidgets(2), reason: 'icon buttons not found');
        expect(_prevIcon, findsOneWidget, reason: 'go to previous btn not found');
        expect(_nextIcon, findsOneWidget, reason: 'go to next btn not found');
      });
    });

    testWidgets('turn of the months - should display date range including months',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();
        final _date = DateTime(2020, 4, 30);

        await initializeWeeklyChartNavigation(tester, SelectedPeriod(
          _date.subtract(Duration(days: 3)),
          _date.add(Duration(days: 3)),
        ));

        final _dateRange = find.text('Apr 27 - May 3');

        expect(_dateRange, findsOneWidget, reason: 'date range is incorrect');
      });
    });

    testWidgets('should disable going forward if end date equal or after today',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();
        final _date = DateTime.now();
        final _selectedPeriod = SelectedPeriod(
          _date.subtract(Duration(days: 3)),
          _date.add(Duration(days: 3)),
        );

        await initializeWeeklyChartNavigation(tester, _selectedPeriod);

        final _initialRangeText = formatDateRange(_selectedPeriod);

        expect(find.text(_initialRangeText), findsOneWidget,
          reason: 'date range is incorrect');

        await tester.tap(find.byIcon(Icons.keyboard_arrow_right));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.text(_initialRangeText), findsOneWidget,
          reason: 'date range did change');
      });
    });

    testWidgets('back and forth arrows should change period by a week',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();
        final _date = DateTime(2021, 1, 15);

        final _selectedPeriod = SelectedPeriod(
          _date.subtract(Duration(days: 3)),
          _date.add(Duration(days: 3)),
        );

        await initializeWeeklyChartNavigation(tester, _selectedPeriod);

        expect(find.text('Jan 12 - 18'), findsOneWidget,
          reason: 'date range is incorrect');

        await tester.tap(find.byIcon(Icons.keyboard_arrow_left));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.text('Jan 5 - 11'), findsOneWidget,
          reason: 'date range is incorrect');

        await tester.tap(find.byIcon(Icons.keyboard_arrow_right));
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.text('Jan 12 - 18'), findsOneWidget,
          reason: 'date range is incorrect');
      });
    });
  });

  group('"formatDateRange"', () {
    test('should properly display date range within a month', () {
      final _date = DateTime(2021, 1, 15);

      final _selectedPeriod = SelectedPeriod(
        _date.subtract(Duration(days: 3)),
        _date.add(Duration(days: 3)),
      );

      expect(formatDateRange(_selectedPeriod), 'Jan 12 - 18',
        reason: 'incorrectly formatted date range');
    });

    test('should properly display date range for a turn of months', () {
      final _date = DateTime(2021, 1, 1);

      final _selectedPeriod = SelectedPeriod(
        _date.subtract(Duration(days: 3)),
        _date.add(Duration(days: 3)),
      );

      expect(formatDateRange(_selectedPeriod), 'Dec 29 - Jan 4',
        reason: 'incorrectly formatted date range');
    });
  });
}


Future initializeWeeklyChartNavigation(WidgetTester tester, SelectedPeriod selectedPeriod)
  async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState(
      WaterEntriesState.initial(),
      UrineEntriesState.initial(),
      UserData.initialize(),
      SummarySettings(
        null,
        selectedPeriod,
      ),
    ),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => Card(
          child: WeeklyChartNavigation()
        ),
      })),
    Duration(seconds: 1000),
  );
}
