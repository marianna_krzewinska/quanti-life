import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:quanti/entries/entries_view.dart';
import 'package:quanti/home/home_view.dart';
import 'package:quanti/main_state.dart';
import 'package:quanti/summary/widgets/summary_view.dart';
import 'package:quanti/urine/widgets/urine_main_tile.dart';
import 'package:quanti/user/user_settings_view.dart';
import 'package:quanti/water/widgets/water_main_tile.dart';


void main() {
  group('"HomeView"', () {
    testWidgets('should display all HomeView title, navigation and main entry tiles',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeHomeViewNavigation(tester);

        expect(find.text('Today'), findsOneWidget,
          reason: 'Home title not displayed');

        expect(find.byIcon(Icons.bar_chart), findsOneWidget,
          reason: 'button to go to charts not displayed');
        expect(find.byIcon(Icons.list), findsOneWidget,
          reason: 'button to go to entries not displayed');
        expect(find.byIcon(Icons.settings), findsOneWidget,
          reason: 'button to go to settings not displayed');

        expect(find.byType(WaterMainTile), findsOneWidget,
          reason: 'water main tile not displayed');
        expect(find.byType(UrineMainTile), findsOneWidget,
          reason: 'urine main tile not displayed');
      });
    });

    testWidgets('tap at chart button should navigate to summary view',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeHomeViewNavigation(tester);

        final _chartsBtn = find.byIcon(Icons.bar_chart);

        expect(_chartsBtn, findsOneWidget,
          reason: 'button to go to charts not displayed');

        await tester.tap(_chartsBtn);
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.text('Summary'), findsOneWidget,
          reason: 'did not navigate to summary');
      });
    }, skip: true); // Skipped due to chart overflow error

    testWidgets('tap at list button should navigate to entries list view',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeHomeViewNavigation(tester);

        final _listBtn = find.byIcon(Icons.list);

        expect(_listBtn, findsOneWidget,
          reason: 'button to go to entries list not displayed');

        await tester.tap(_listBtn);
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.text('Entries'), findsOneWidget,
          reason: 'did not navigate to entries list view');
      });
    });

    testWidgets('tap at settings button should navigate to settings view',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeHomeViewNavigation(tester);

        final _settingsBtn = find.byIcon(Icons.settings);

        expect(_settingsBtn, findsOneWidget,
          reason: 'button to go to settings not displayed');

        await tester.tap(_settingsBtn);
        await tester.pumpAndSettle(Duration(seconds: 2));

        expect(find.text('Settings'), findsOneWidget,
          reason: 'did not navigate to settings view');
      });
    });
  });
}


// DATA & helper functions


Future initializeHomeViewNavigation(WidgetTester tester)
  async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
    middleware: [
      NavigationMiddleware<AppState>(),
    ]
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      navigatorKey: NavigatorHolder.navigatorKey,
      initialRoute: '/',
      routes: {
        '/': (_) => HomeView(),
        SummaryView.route: (_) => SummaryView(),
        EntriesView.route: (_) => EntriesView(),
        UserSettingsView.route: (_) => UserSettingsView(),
      })),
    Duration(seconds: 1000),
  );
}
