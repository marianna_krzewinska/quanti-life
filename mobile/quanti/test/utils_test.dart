import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';

import 'package:quanti/utils.dart';


void main() {
  group('"getHour"', () {
    test('should format DateTime object to HH:mm', () {
      DateTime date = DateTime(2020, 3, 14, 1, 12);
      String expectedText = '01:12';
      String actualText = getHour(date);

      expect(actualText, expectedText, reason: 'time is incorrectly formatted');
    });
  });

  group('"formatTime"', () {
    test('should format TimeOfDay object to HH:mm', () {
      TimeOfDay time = TimeOfDay(hour: 1, minute: 12);
      String expectedText = '01:12';
      String actualText = formatTime(time);

      expect(actualText, expectedText, reason: 'time is incorrectly formatted');
    });
  });

  group('"formatDate"', () {
    test('should format date to "EEE, MMM d" format', () {
      DateTime date = DateTime(2020, 3, 14, 1, 12);
      String expectedText = 'Sat, Mar 14';
      String actualText = formatDate(date);

      expect(actualText, expectedText, reason: 'date is incorrectly formatted');
    });
  });

  group('"justDate"', () {
    test('should remove time from date', () {
      DateTime date = DateTime(2020, 3, 14, 1, 12);
      DateTime newDate = justDate(date);

      expect(date.year, newDate.year, reason: 'year doesn\'t match');
      expect(date.month, newDate.month, reason: 'month doesn\'t match');
      expect(date.day, newDate.day, reason: 'day doesn\'t match');
      expect(newDate.hour, 0, reason: 'hour is not 0');
      expect(newDate.minute, 0, reason: 'minute is not 0');
      expect(newDate.second, 0, reason: 'second is not 0');
      expect(newDate.millisecond, 0, reason: 'millisecond is not 0');
      expect(newDate.microsecond, 0, reason: 'microsecond is not 0');
    });
  });
}
