
import 'package:redux/redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:quanti/login_view.dart';
import 'package:quanti/main_state.dart';

import 'utils.dart';

void main() {
  group('"LoginView"', () {
    testWidgets('Should display login title and button',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await setupEnvironment();

        await initializeLoginView(tester);

        final loginTitle = find.text('Login');
        final loginButton = find.byType(TextButton);

        expect(loginTitle, findsOneWidget, reason: 'title is incorrect');
        expect(loginButton, findsOneWidget, reason: 'login button not found');
      });
    });
  });
}


// DATA & helper functions


Future initializeLoginView(WidgetTester tester) async {
  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Quanti.life',
      initialRoute: LoginView.route,
      routes: {
        LoginView.route: (_) => LoginView(),
      })),
    Duration(seconds: 1000),
  );
}
