import 'package:flutter_test/flutter_test.dart';

import 'package:quanti/entries/date_row.dart';
import 'package:quanti/entries/entries_view.dart';
import 'package:quanti/urine/urine_model.dart';
import 'package:quanti/urine/urine_utils.dart';
import 'package:quanti/urine/widgets/urine_entry_tile.dart';
import 'package:quanti/utils.dart';
import 'package:quanti/water/water_model.dart';
import 'package:quanti/water/widgets/water_entry_tile.dart';


void main() {
  group('"generateListOfEntries"', () {
    test('should return list of tile components in chronological order', () {
      final _date = DateTime.now();
      final _earlierDate = _date.subtract(Duration(minutes: 10));

      final _waterEntry = WaterEntry('id', 100, _date, _date, _date);
      final _urineEntry = UrineEntry(
        'id', 300, urineColors[1].id, _earlierDate, _earlierDate, _earlierDate);

      final widgets = generateListOfEntries(EntriesMap([_waterEntry], [_urineEntry]));

      expect(widgets.length, 2, reason: 'did not return 2 widgets');
      expect(widgets[0] is WaterEntryTile, true, reason: 'first widget is incorrect');
      expect(widgets[1] is UrineEntryTile, true, reason: 'second widget is incorrect');
    });

    test('should return inserted DateRows for each changed date', () {
      final _today = DateTime.now();
      final _yesterday = justDate(_today).subtract(Duration(days: 1));
      final _earlierDate = _yesterday.subtract(Duration(days: 7));

      final _waterEntry = WaterEntry('id', 100, _today, _today, _today);
      final _urineEntry1 = UrineEntry(
        'id', 300, urineColors[1].id, _yesterday, _yesterday, _yesterday);
      final _urineEntry2 = _urineEntry1.update(newTimestamp: _earlierDate);

      final widgets = generateListOfEntries(EntriesMap(
        [_waterEntry],
        [_urineEntry1, _urineEntry2]));

      expect(widgets.length, 5, reason: 'did not return 5 widgets');
      expect(widgets[0] is WaterEntryTile, true, reason: 'first widget is incorrect');
      expect(widgets[1] is DateRow, true, reason: 'second widget is incorrect');
      expect(widgets[2] is UrineEntryTile, true, reason: 'third widget is incorrect');
      expect(widgets[3] is DateRow, true, reason: 'fourth widget is incorrect');
      expect(widgets[4] is UrineEntryTile, true, reason: 'fifth widget is incorrect');
    });
  });

  // TODO: #12 Write tests for EntriesView (Marianna Krzewińska)
  group('"EntriesView"', () {}, skip: true);
}
