import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:quanti/entries/date_row.dart';
import 'package:quanti/utils.dart';


void main() {
  group('"DateRow"', () {
    testWidgets('should display "Yesterday" for yesterday at 00:00',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        final _yesterday = justDate(DateTime.now()).subtract(Duration(days: 1));

        await initializeDateRow(tester, _yesterday);

        expect(find.text('Yesterday'), findsOneWidget, reason: 'date text not found');
      });
    });

    testWidgets('should display nothing for any time/date after today midnight',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        final _today = DateTime.now();

        await initializeDateRow(tester, _today);

        expect(find.byType(Text), findsNothing, reason: 'date text found');
      });
    });

    testWidgets('should display formatted date for any other day',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        final _date = DateTime(2021, 3, 12, 22, 14, 54);

        await initializeDateRow(tester, _date);

        expect(find.text('Fri, Mar 12'), findsOneWidget, reason: 'date text not found');
      });
    });
  });
}


// DATA & helper functions


Future initializeDateRow(WidgetTester tester, DateTime date) async {
  await tester.pumpWidget(MaterialApp(
      title: 'Quanti.life',
      initialRoute: '/',
      routes: {
        '/': (_) => DateRow(date: date),
      }),
    Duration(seconds: 1000),
  );
}
