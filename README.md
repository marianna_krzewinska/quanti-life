# Quanti.life

[![pipeline status](https://gitlab.com/marianna_krzewinska/quanti-life/badges/master/pipeline.svg)](https://gitlab.com/marianna_krzewinska/quanti-life/-/commits/master)
[![coverage report](https://gitlab.com/marianna_krzewinska/quanti-life/badges/master/coverage.svg)](https://gitlab.com/marianna_krzewinska/quanti-life/-/commits/master)

## About

### Hypothesis

Hydration affects how we feel and drinking more (staying more hydrated) will make us feel better.

### Theory

The goal is to track hydration by measuring water intake and peeing.

The hydration level is a result of the amount of water consumed and excreted. Excretion through skin and breath is a part of water loss, but it's impossible to measure it directly. It should be relatively stable, and any changes will be related to other factors that we can track, like e.g. exercises.

Urine color is the best external indicator of hydration - the lighter it is, the more the body is hydrated.

Water is consumed only through conscious consumption (drinking and with food), but it doesn't represent hydration levels. It's just a way to increase your hydration. Beverages vary in hydrating capabilities, but for the time being, the composition of a beverage will be ignored, and the volume of water in a drink will be assumed to be equal to the drink volume.

By analyzing water intake resulting in lighter urine, we'll see how much drinking is needed to maintain healthy levels of hydration.

### Future

This app will include more tracked subjects e.g. food, symptoms, cognitive tests, HRV, etc. - the data model will take it into account.


## Development setup

### Prerequisits

- Docker
- Docker Compose
- OS: Linux (verified), MacOS/Windows (usb connection path might not work correctly - but it [should be possible to setup](https://docs.sevenbridges.com/docs/mount-a-usb-drive-in-a-docker-container))

### How to start

1. Fill in .env file with PostgreSQL connection data - database won't be created automatically, this will require connecting to it after build and doing it manually (step 4.)
2. Run `docker-compose up` in the top directory - it will build and start development environment both for mobile and server part.
   1. If you'd like to develop just one of the parts, run `docker-compose up [mobile/server/postgres]` - you can also pass a combination, e.g. `docker-compose up server postgres` for only server sevelopment
3. After build there will be two IDEs started:
   1. **mobile IDE** available at localhost:8080
   2. **server IDE** available at localhost:8000
4. To create a database connect to the postgres container

### Server

1. Fill in .env file and create database in PostgreSQL
2. Reload, and press `F5` - server should start at `localhost:8001` (port defined in .env or passed otherwise through env variables)

### [Mobile app](mobile/README.md)

## Python Guidelines

1. When installing new package, add it to `requirements.in`, then run `pip-compile requirements.in` to lock the version in `requirements.txt`. Both development and production build depend on versions in requirements.txt

2. Migrations
  - `python -m alembic upgrade head` - runs migrations
  - `python -m alembic revision -m "migration description"` - creates new migration script (puts it into `migartions/versions/`)

## Notes from and for development

### Mobile App Environments

Mobile app can be built per environment - configurations are defined in folder `./mobile/quanti/lib/configs`. To build an app for given environemnt (currently supoorted `local` and `dev`):

1. go to the `./mobile/quanti` folder .
2. run `flutter build apk -t main-[local or dev].dart`.
    - `-t` flag tells flutter which file to use as an app entry point. Each environment has its own entry file.
3. After build the apk files will be available in folder `./mobile/quanti/build/app/outputs/flutter-apk`.
4. Flutter team suggests to use the `app-relese.apk` file.

### Deployment

Kubernetes is deployed on Digital Ocean using their Kubernetes, Databases and Container Registry services with NGINX Ingress Controller (from DO marketplace).

Server deployment configuration is stored in `./server/deployment` folder. Helm is used for template management and deployment versioning.

The only missing configuration file is a secret storing database credentials. It was created manually using command `kubectl create secret generic tracker-server-db --from-literal password=[db password]`. The DB name and user name are configured in `values.[env].yaml` (currently supported `dev`).

Here are some useful links:

1. [K8s TSL certificate manager howto (Let's Encrypt)](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes)
